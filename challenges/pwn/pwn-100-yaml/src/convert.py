#!/usr/bin/env python2

import pickle
import os
import SocketServer
import yaml

class Handler(SocketServer.BaseRequestHandler, SocketServer.ForkingMixIn):
    def handle(self):
        os.dup2(self.request.fileno(), 1)
        os.dup2(self.request.fileno(), 2)
        self.request.sendall(pickle.dumps(yaml.load(self.request.recv(4096))))

server = SocketServer.TCPServer(('0.0.0.0', 2323), Handler)
server.serve_forever()
