FROM alpine

# We need some basic build tools first off
RUN apk add build-base
# We also need the kernel headers - hopefully these are similar enough to the
# host to not explode on impact
RUN apk add linux-headers
# We need Python 2 to build unicorn, Python 3 and some dev libs for angr 
RUN apk add python2 python3 python3-dev libffi-dev bash

# We should be able to install unicorn using pip and pointing it at Python 2
# This is a workaround for macOS documented in the angr installation guide
RUN UNICORN_QEMU_FLAGS="--python=$(which python2)" pip3 install unicorn

# We install z3 and have to manually make a symlink for libz3.so
RUN apk add z3 py3-z3
RUN ln -sf libz3.so.4.8 /usr/lib/libz3.so
# Finally we can install angr and a hardcoded set of dependencies because we
# can't trust it to not pull in z3 and fuck us by building it for 3000 years
COPY ./angr-requirements.txt /tmp/
RUN pip3 install --no-deps -r /tmp/angr-requirements.txt angr==8.19.2.4

# In order for angr to load the unicorn engine it needs to find some libs which
# it can't seem to find when they're in the Python 3 site-packages dirs
RUN ln -s "/usr/lib/python3*/site-packages/unicorn/lib/libunicorn.so" "/usr/lib/"
RUN ln -s "/usr/lib/python3*/site-packages/pyvex/lib/libpyvex.so" "/usr/lib/"

# After all that we still need to fix stuff because claripy tries to do nasty
# things to the internals of z3 - we also need to add GNU patch for this
# because the one in busybox is trash and doesn't do anything or error out :/
COPY ./backend_z3.py.patch /tmp
RUN apk add patch
RUN patch /usr/lib/python3*/site-packages/claripy/backends/backend_z3.py /tmp/backend_z3.py.patch 
