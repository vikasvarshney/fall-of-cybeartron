# Cybears CTF

This repo contains the CTF challenges used in the Fall of Cybeartron CTF, held
at BSides Canberra 2019..

## Running challenges locally

Registry info at: [fall-of-cybeartron/container_registry](https://gitlab.com/cybears/fall-of-cybeartron/container_registry)

The challenges are hosted using kubernetes. For a local deployment we recommend
installing k3s as per [https://k3s.io/]. Once installed you can generate the
kubernetes deployment yaml files.
```bash
bin/manifest_to_k8s_deploy.py [--no-port-reuse] [-f]
```
For local testing you want to set `--no-port-reuse`, and `-f`

Spin these instances up by running the following:
```bash
bin/spin_up_challenges.sh
```

or spin up a single challenge like this
```bash
kubectl create -f .deploy/<challenge_name>.yaml
```

If you need to tear these down do so by running the following:
```bash
bin/spin_down_challenges.sh
```

### Running CTFd

CTFd is hosted using a docker compose script. You need docker-compose installed to do this

```bash
pip install --upgrade docker-compose
```

You will also need to install the Python dependencies listed in
`requirements-infra.txt` to do this.

`bin/spin_up_ctfd.py` will start up a CTFd instance running on docker-compose,
k3s or k8s on top of a Maria and Redis backend. An admin password will either
be sourced from or generated and written into `bin/.ctfd_admin_pass` and used
to set up a well-known `cybears` user which will receive admin rights.

```bash
bin/spin_up_ctfd.py {compose,k3s,k8s} {start,stop,purge}
```
This prints out some export strings that you need to add in order to use following commands.

If using docker-componse you can look at the logs of a running CTFd instance by moving into the
`infra/CTFd` directory and running `docker-compose logs -f`.

Note: The k3s and k8s deployments require some manual steps and k3s in
particular is pretty janky at the moment. We're working on improving this so
that it's possible to spin everything up across our "supported" platforms.

### Uploading Pages
Required files for CTFd need to be uploaded to the runnig CTFd instance.
`bin/upload_pages.py` will upload any required pages.

### Uploading Challenges
`bin/upload_challenges.py` knows how to discover and upload challenges based on
the presence of a MANIFEST.yml in a challenge directory. It does a best effort to 
build any handouts a challenge requires but may not work. An alternative is to download
the handouts from a sucessful gitlab pipeline and put them into the required location.

### Removing Challenges
`bin/remove_challenges.py` will delete all challenges previously uploaded to
CTFd. Useful for clean state when testing.

## Troubleshoting

### K3S

#### Check services (LoadBalancer)
```bash
kubectl get service [-o wide]
```

#### Check pods (Challenges)
```bash
kubectl get pods
```

#### Check status of pod (Challenge)
```bash
kubectl describe pod <pod_name_from_kubectl_get_pods>
```

#### Check logs of a container
```bash
kubectl logs <pod_name_from_kubectl_get_pods> <container>
```
