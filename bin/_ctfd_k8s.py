import os.path
from _k8s import (
    create, delete, wait_for_deployment, get_service_address,
    find_or_prompt_docker_secret
)

def start_ctfd(deployment_dir):
    create(os.path.join(deployment_dir, "deploy.yml"), can_fail=True)
    # We need to add a secret to the ctfd namespace - this prompts the user
    find_or_prompt_docker_secret("regcred", ns="ctfd")
    # We should be able to wait for the rollout now
    wait_for_deployment("ctfd-cache", ns="ctfd")
    wait_for_deployment("ctfd-db", ns="ctfd")
    wait_for_deployment("ctfd-web", ns="ctfd")
    # And now pull the external service address - we always use HTTPS when we
    # deploy on kubernetes thanks to the nginx sidecar container
    return "https://{}/".format(get_service_address("ctfd-web", ns="ctfd"))

def stop_ctfd(deployment_dir):
    # XXX: Need to work out how to take it down and then back up...
    purge_ctfd(deployment_dir)

def purge_ctfd(deployment_dir):
    # Tell the k8s module that it'll be using k3s as an intermediary
    delete(os.path.join(deployment_dir, "deploy.yml"), quiet=False)
