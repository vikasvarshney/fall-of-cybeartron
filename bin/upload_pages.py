#!/usr/bin/env python3
import json
import os
import os.path
import requests.compat

from ctfd_api import CTFdAuthenticatedAPISession

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def upload_page(api_session, page):
    print("Attempting to upload the page content from {}".format(page["content"]))
    # Now we can create the page
    with open(
        os.path.join(SCRIPT_DIR, "..", "infra", "ctfd", "theme", page["content"]), "r"
    ) as pagefile:
        page_content = pagefile.read()

    # Make a new page from scratch
    if page["mode"] == "new":

        # Grab a list of existing pages
        page_get_r = api_session.get("pages")
        assert page_get_r.status_code == 200, page_get_r.content
        response_data = json.loads(page_get_r.content)
        assert response_data["success"] == True, response_data
        # Go hunting for an existing page with the same route
        for page_data in response_data["data"]:
            if page_data["route"] == page["route"]:
                # Delete the existing page
                page_delete_r = api_session.delete(
                    "pages/{}".format(page_data["id"]),
                    json={
                        "page_id": page_data["id"],
                    },
                )
                assert page_delete_r.status_code == 200, page_delete_r.content
                break

        page_post_r = api_session.post(
            "pages",
            json={
                "title": page["title"],
                "route": page["route"],
                "content": page_content,
            },
        )

    # Edit an existing page (assuming you know the page id)
    elif page["mode"] == "edit":
        page_post_r = api_session.patch(
            "pages/{}".format(page["id"]),
            json={
                "page_id": page["id"],
                "content": page_content,
            },
        )

    assert page_post_r.status_code == 200, page_post_r.content
    response_data = json.loads(page_post_r.content)
    page_id = response_data["data"]["id"]

    if page["mode"] == "new":
        print("Uploaded {} as page with id {}".format(page["route"], page_id))
    elif page["mode"] == "edit":
        print("Updated page with id {}, resulting id: {}".format(page["id"], page_id))

if __name__ == "__main__":

    admin_user = "cybears"
    CTFD_HOST = os.environ.get("CTFD_HOST", "http://localhost:8000/")

    with open(os.path.join(SCRIPT_DIR, ".ctfd_admin_pass"), "r") as passfile:
        admin_pass = passfile.read().strip()

    api_session = CTFdAuthenticatedAPISession(CTFD_HOST, admin_pass)

    # mode = new/edit
    pages_list = [
        {"mode": "new", "title": "Rules", "route": "rules", "content": "pages_rules.md"},
        {"mode": "new", "title": "Story", "route": "story", "content": "pages_story.md"},
        {"mode": "edit", "id": "1", "content": "pages_welcome.html"},
    ]

    for page in pages_list:
        upload_page(api_session, page)
