from Crypto import Random
from Crypto.Cipher import AES
import hashlib
import math
from binascii import *

import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--inputfile', type=str,  default="", help='File to Encrypt/Decrypt', required=True)
parser.add_argument('-p', '--passphrase', type=str,  default="", help='passphrase to encrypt/decrypt', required=True)
parser.add_argument('-m', '--mode', type=str,  default="encrypt", help='encrypt|decrypt', required=True)
args = parser.parse_args()

input_file = args.inputfile
passphrase = args.passphrase
mode = args.mode

##check modes
valid_modes = ["encrypt", "decrypt", "ENCRYPT", "DECRYPT"]
if mode not in valid_modes:
    print("ERROR: invalid mode. Select either 'encrypt' or 'decrypt'")
    exit(-1)

##check passphrase
if passphrase =="" or passphrase == None:
	print("ERROR: passphrase required")
	exit(-1)

#Open input file
if input_file == "":
    print("ERROR: input_file blank")
    exit(-1)

try:
    f = open(input_file, 'rb')
    input_data = f.read()
    f.close()
except:
    print("ERROR: Could not open input_file")
    exit(-1)


SESSION_SIZE = 8    # In AES.block_size units; so 8 -> 8*16*8 = 1024 bits = 128 bytes
PASS_ITERS = 100000 # number of times to iterate sha256 to turn a passphrase into an AES key

def incrementBytes(b):
  # Just adds one to a bytes object; useful for using AES in counter mode
  bArray = [ b[i] for i in range(len(b)) ]
  bArray.reverse()
  for i in range(len(bArray)):
    if bArray[i] != 255:
      bArray[i] = bArray[i]+1
      break
    else:
      bArray[i] = 0x0
  bArray.reverse()
  return bytes(bArray)

def passphraseToKey(passphrase):
  myKey = passphrase.encode()
  for i in range(PASS_ITERS):
    myKey = hashlib.sha256(myKey).digest()
  return myKey

def padMessage(message,padChar):
  if len(padChar) != 1:
    print("Error in padMessage : pad character is not a single character")
    return ''
  if (ord(padChar) < 0x20) or (ord(padChar) > 126) :
    print("Error in padMessage : pad character is not printable")
    return ''
  padLength = SESSION_SIZE * AES.block_size - (len(message) % (SESSION_SIZE*AES.block_size))
  return message + padLength*padChar

def encryptSession(session,key,IV):
  plainBlocks = [ session[AES.block_size*i : AES.block_size*(i+1)] for i in range(SESSION_SIZE) ]
  encryptedBlocks = []
  for p in plainBlocks:
    plainArray = [p[i] for i in range(len(p))]
    # Produce the key stream to xor to the plaintext
    xorKey = AES.new(key,AES.MODE_ECB).encrypt(IV)
    xorKeyArray = [ xorKey[i] for i in range(len(xorKey)) ]
    # In CTR mode, we just increment the IV by 1
    IV = incrementBytes(IV)
    # Xor the key to the correct part of the session and append to encryptedBlocks
    thisBlock = [ xorKeyArray[i] ^ plainArray[i] for i in range(len(p)) ]
    encryptedBlocks.append(bytes(thisBlock))
  encSession = b''
  for b in encryptedBlocks:
    encSession += b
  return encSession

def encrypt(strmessage,passphrase):
  if type(strmessage) == str:
    message = bytes(strmessage.encode('utf-8'))
  else:
    message = strmessage
  myKey = passphraseToKey(passphrase)
  numBlocks = math.ceil(len(message) / AES.block_size)
  # Firstly, we pad to the nearest session size
  paddedMessage = padMessage(message,b'_')
  numSessions = math.ceil(numBlocks / SESSION_SIZE)
  plainSessions = [ paddedMessage[ SESSION_SIZE*AES.block_size*i : SESSION_SIZE*AES.block_size*(i+1)] for i in range(numSessions)]
  # Initialize list of sessions to return
  encSessions = []
  # Select a good IV of length AES_BLOCK_SIZE bytes
  IV = Random.new().read(AES.block_size)
  # Set aside this IV so we can return it with the encrypted message
  oldIV = IV
  # Encrypt sessions, and append to encSessions list
  for p in plainSessions:
    encSessions.append( encryptSession(p , myKey, IV) )
    # In CTR mode, we just increment the IV by 1
    IV = incrementBytes(IV)
  # Return the concatenation of all the encrypted sessions
  returnMe = b''
  for e in encSessions:
    returnMe += e
  return oldIV,returnMe

def decrypt(IV,strmessage,passphrase):
  # To decrypt, we just re-encrypt, but with the preset IV
  if type(strmessage) == str:
    message = bytes(strmessage.encode('utf-8'))
  else:
    message = strmessage
  myKey = passphraseToKey(passphrase)
  numBlocks = math.ceil(len(message) / AES.block_size)
  # Firstly, we pad to the nearest session size
  paddedMessage = padMessage(message,b'_')
  numSessions = math.ceil(numBlocks / SESSION_SIZE)
  plainSessions = [ paddedMessage[ SESSION_SIZE*AES.block_size*i : SESSION_SIZE*AES.block_size*(i+1)] for i in range(numSessions)]
  # Initialize list of sessions to return
  encSessions = []
  # Encrypt sessions, and append to encSessions list
  for i in range(numSessions):
    encSessions.append( encryptSession(plainSessions[i] , myKey, IV) )
    # In CTR mode, we just increment the IV by 1
    IV = incrementBytes(IV)
  # Return the concatenation of all the encrypted sessions
  returnMe = b''
  for e in encSessions:
    returnMe += e
  return returnMe

if __name__ == "__main__":

	if mode.lower() == "encrypt":
		#print("DEBUG: Encrypting")

		iv, cipher = encrypt(input_data, passphrase)

		j = {"IV": hexlify(iv).decode('utf-8'), "cipher": hexlify(cipher).decode('utf-8')}
		print(json.dumps(j))

	if mode.lower() == "decrypt":
		#print("DEBUG: Decrypting")

		try:
			j = json.loads(input_data)
		except:
			print('ERROR: Failed to load encrypted data as json, should be {"IV": "iv in hex", "cipher":"cipher in hex"}')
			exit(-1)

		iv = unhexlify(j["IV"])
		cipher = unhexlify(j["cipher"])

		#print("DEBUG: [" + passphrase + "]")

		d = decrypt(iv,cipher,passphrase)

		print("DECRYPTED: " + str(d))