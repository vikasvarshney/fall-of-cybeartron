# Stevedore Level1

* title: Stevedore Level 1
* difficulty: easy/medium
* tags: misc docker forensics
* todo: Automate pushing level1a/level1b/level2 to bitbucket

## WARNING

Because the image is going to be publically available from docker hub, the deploy script
can't run until the day of the competition.

## Infrastructure

Push the level1a and level1b folders (including Dockerfiles) to cybears bitbucket
Docker hub detects the push, and auto-builds the containers
Users visit docker hub to view the Dockerfiles / images
Users pull the containers and have to find the flags on their own disk

## Skills required

* docker installation
* reading a Dockerfile on docker hub
* pulling an image from docker hub
* spelunking the layers that make up a docker image
* XORing two binary pieces of data

## How to test drive

1. `docker build -t cybears/level1a ./level1a/`
2. `docker build -t cybears/level1b ./level1b/`
3. Pretend you just pulled your local copies of cybears/level1{a,b} from docker hub
4. You can read the Dockerfiles in `level1a/Dockerfile` and `level1b/Dockerfile`
5. Run the container: `docker run cybears/level1a`
6. Or jump into a shell: `docker run cybears/level1a /bin/sh`
7. ???

## Solution

The second docker challenge is split into two halves.

Level1a contains a flag.txt (binary data) that is deleted by a Dockerfile instruction. 
As such, the file is still present in a layer of the image. Depending on the user's 
operating system and Docker backing filesystem, they might need to browse to a system
folder, ungzip some files, or a couple of other options, in order to recover the file.

Level1b contains a Dockerfile directive that appears to copy a 'flag.txt' file into a
container, but in reality 'flag.txt' in this case is a folder. As such, numerous other
files are copied into the container, including a fake `busybox ` binary and a file called
`. ` hidden in the /bin folder.

Once the user realises a bunch of extra files have appeared in the container layer 
associated with that command (visible in `docker history` as well, since it copies a path)
they will have to hunt to find the binary data - it is currently in `. ` but I'm open 
to less dumb suggestions for hiding it.

Once they have the two parts, they have to XOR them together to get the plaintext key. 
There is a hint to do this in the README (relating to combining the flags to make 
things clear). 

The solve script simply pulls the real flag source files from each part, and runs the 
XOR manually.

## Flag

cybears{tru$t_in_th3_c0nta!n3rz}
