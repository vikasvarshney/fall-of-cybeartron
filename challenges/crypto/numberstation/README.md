# Number station

This challenge is an FM modulated number station that broadcasts
public domain music, overlaid with a sequence of spoken letters
and digits.

---

# Flag

- `cybears{th1s_i5_judg3_judy}`

# Solution

The station can be decoded using GNU Radio. The player
then needs to write down the sequence of numbers/letters
the voice reads.

This stream is a base32 encoded stream, this can be
determined using the alphabet and the four equals
at the end.

After base32 decoding the stream, the player is left
with an Vigenere encrypted message. The player can
use frequency analysis to determine the key length
then attempt to break each ceasar cipher until they
regonise some words.

Once decrypted, the message reveals the flag.
