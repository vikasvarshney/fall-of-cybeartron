#!/bin/bash

IMAGEFILE=bearmetl.img
VOLUMENAME=CYBEARMETAL
VOLUMEID=aabbccdd
FLAG=flag.txt
FLAG_FILE_ON_DISK=GARBAGE.TXT
HANDOUT=handout
ENCRYPTED_FLAG=encrypted_flag.bin

# TODO: make sure this is the same key as in main.asm
# might generate this in the future
KEY=ZvbXrp1-

echo Removing cruft
rm $IMAGEFILE mbr.bin main.bin $ENCRYPTED_FLAG
rm -rf export/handout/*

echo Creating disk image
dd if=/dev/urandom of=$IMAGEFILE bs=512 count=2880

echo Formatting disk image
mkfs.msdos -F 12 --invariant -n $VOLUMENAME -i $VOLUMEID $IMAGEFILE

echo Encrypting flag
python2 ./encrypt.py $ENCRYPTED_FLAG $FLAG $KEY

echo Copying flag file
mcopy -i $IMAGEFILE $ENCRYPTED_FLAG ::$FLAG_FILE_ON_DISK

echo Building main code
nasm -f bin -o main.bin main.asm

echo Building MBR code
nasm -f bin -o mbr.bin mbr.asm

echo Patching MBR code into disk image
python2 ./patch.py $IMAGEFILE mbr.bin 3E 1C0

echo Patching main code into disk image
python2 ./patch.py $IMAGEFILE main.bin 400

echo Making handout
mkdir -p export/handout
cp $IMAGEFILE "export/handout/$IMAGEFILE"