#!/usr/bin/env python3
import argparse
import json
import os
import os.path
import pathlib
import sys
import yaml
from string import Template

from challenge import Challenge
from ctfd_api import CTFdAuthenticatedAPISession

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DEPLOY_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, "..", ".deploy"))

CTFD_HOST = os.environ.get("CTFD_HOST", "http://localhost:8000/")

def discover_manifests(paths):
    for path in paths:
        for name, _, files in os.walk(path.absolute()):
            if "MANIFEST.yml" in files:
                manifest_path = os.path.join(path, name, "MANIFEST.yml")
                yield os.path.realpath(manifest_path)


def get_server_details(deployment_yaml):
    dns = None
    port = None
    with open(deployment_yaml) as deployment:
        for data in yaml.load_all(deployment):
            metadata = data.get("metadata")
            if metadata:
                annotations = metadata.get("annotations")
                if annotations:
                    dns = annotations.get("cybears_dns")

            spec = data.get("spec")
            if spec:
                ports = spec.get("ports")
                if ports:
                    port = ports[0].get("port")
    return (dns,port)

def find_k8s_deploy(challenge_obj):
    k8s_deploy = os.path.join(
        DEPLOY_DIR, challenge_obj.deploy_name + ".deployment.yaml"
    )
    return k8s_deploy if os.path.exists(k8s_deploy) else None
        
def get_server_details_for_challenge(challenge_obj):
    k8s_deploy = find_k8s_deploy(challenge_obj)
    return get_server_details(k8s_deploy) if k8s_deploy else None

def find_or_create_challenge(api_session, challenge_obj, server_details):
    """
    Create a new challenge for the specified object or update an existing one.
    """
    # Attempt to find an existing challenge with the same name. NOTE: this will
    # duplicate challenges if the name is changed for some reason.
    chall_get_r = api_session.get("challenges")
    assert chall_get_r.status_code == 200, chall_get_r.content
    response_data = json.loads(chall_get_r.content)
    assert response_data["success"] == True, response_data
    # Go hunting for an existing challenge with the same name
    for challenge_data in response_data["data"]:
        if challenge_data["name"] == challenge_obj.name:
            # Break out to avoid reuploading the challenge again
            print("Existing challenge found with id {!r}".format(
                challenge_data["id"]
            ))
            api_method = api_session.patch
            api_endpoint = "challenges/{}".format(challenge_data["id"])
            break
    else:
        api_method = api_session.post
        api_endpoint = "challenges"
    # We may need to substitute some deployment bits in the description field
    description = challenge_obj.description
    if server_details:
        description = Template(challenge_obj.description).substitute(
            target_dns = server_details[0],
            target_port = str(server_details[1])
        )
    # Now we can construct the JSON we want to send to the API to either add or
    # update the challenge depending on trivia or regular
    dc_value, dc_decay, dc_min = get_dynamicchallenge_values(challenge_obj.category)
    chal_r = api_method(
        api_endpoint,
        json={
            "type": "dynamic",     # Use "standard" for regular scoring
            "state": "visible",
            "name": challenge_obj.name,
            "description": description,
            "category": challenge_obj.category,
            "value": dc_value,
            "decay": dc_decay,
            "minimum": dc_min,
        }
    )
    assert chal_r.status_code == 200, chal_r.content
    response_data = json.loads(chal_r.content)
    challenge_data = response_data["data"]
    return challenge_data

def load_manifest(manifest_path):
    print("Loading challenges from {}".format(manifest_path))
    yield from Challenge.from_manifest(manifest_path)

def remove_challenge_subobjects(api_session, chal_id, api_name):
    # Delete all of the `<api_name>` things!
    get_r = api_session.get("challenges/{}/{}".format(chal_id, api_name))
    assert get_r.status_code == 200, get_r.content
    response_data = json.loads(get_r.content)
    assert response_data["success"] == True, response_data
    for obj in response_data["data"]:
        # We need to send an `application/json` request so just jam an empty
        # string in as the `json` kwarg to make that happen
        del_r = api_session.delete(
            "{}/{}".format(api_name, obj["id"]), json=""
        )
        assert del_r.status_code == 200, del_r.content

def upload_challenge(api_session, challenge, force=False):
    print("Want to upload %r" % challenge.name)
    if not challenge.use and not force:
        print("Refusing to upload {!r} without --force".format(challenge.name))
        return
    server_details = get_server_details_for_challenge(challenge)
    challenge_data = find_or_create_challenge(api_session, challenge, server_details)
    challenge_id = challenge_data["id"]
    # Delete any existing files before uploading the ones we have now - this is
    # the nuclear option which is probably fine...
    remove_challenge_subobjects(api_session, challenge_id, "files")
    for handout in challenge.handouts:
        print("Uploading handout: {!r}".format(handout.file.name))
        file_post_r = api_session.post(
            "files",
            # We use form data here to pack in the files to upload
            data={
                "nonce": api_session.nonce,
                "type": "challenge",
                "challenge_id": challenge_id,
            },
            files={"file": handout.file},
        )
        assert file_post_r.status_code == 200, file_post_r.content
    # Delete any old flags and add new ones
    remove_challenge_subobjects(api_session, challenge_id, "flags")
    for flag in challenge.flags:
        flag_post_r = api_session.post(
            "flags",
            json={
                "challenge": challenge_id,
                **flag,
            },
        )
        assert flag_post_r.status_code == 200, flag_post_r.content
    # add tags
    remove_challenge_subobjects(api_session, challenge_id, "tags")
    for tag in challenge.tags:
        tag_post_r = api_session.post(
            "tags",
            json={
                "challenge_id":challenge_id,
                "value":tag,
            },
        )

def get_dynamicchallenge_values(category):
    if category == "trivia":
        return args.trivia_initial, args.trivia_decay, args.trivia_min
    else:
        return args.score_initial, args.score_decay, args.score_min

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "search_path", nargs="*", type=pathlib.Path,
        default=(pathlib.Path(__file__).parent.parent / "challenges", ),
    )
    parser.add_argument("--force", "-f", action="store_true")
    parser.add_argument('--score_initial', nargs='?', type=int, default=500)
    parser.add_argument('--score_decay', nargs='?', type=int, default=4)
    parser.add_argument('--score_min', nargs='?', type=int, default=50)
    parser.add_argument('--trivia_initial', nargs='?', type=int, default=100)
    parser.add_argument('--trivia_decay', nargs='?', type=int, default=4)
    parser.add_argument('--trivia_min', nargs='?', type=int, default=10)
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    with open(os.path.join(SCRIPT_DIR, ".ctfd_admin_pass"), "r") as passfile:
        admin_pass = passfile.read()
    api_session = CTFdAuthenticatedAPISession(CTFD_HOST, admin_pass)
    for manifest_path in discover_manifests(args.search_path):
        for challenge_obj in load_manifest(manifest_path):
            upload_challenge(api_session, challenge_obj, force=args.force)
