#!/usr/bin/env bash

MY_PATH=`readlink -f "$0"`
ROOT_DIR="`dirname ${MY_PATH}`/../"
ROOT_DIR="`readlink -f ${ROOT_DIR}`"
echo "[+] Spinning down :("
kubectl delete -f "${ROOT_DIR}/.deploy"
