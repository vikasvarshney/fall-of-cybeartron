#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <assert.h>
#include <dlfcn.h>
#include <sys/mman.h>
#include <string.h>
#include <time.h>

#include "blockdude.h"

#define RAND_RANGE(min, max) ((rand() % ((max) + 1 - (min)) + (min)) & 0xFF)

void *rand_address()
{
    srand(time(NULL));
    u32 ret = 0x0;

    // second block: 6<=y<=17, 3<=x<=17
    ret |= RAND_RANGE(6, 17) << 24;
    ret |= RAND_RANGE(3, 17) << 16;

    // first block: 3<=y<=5, y+1<=x<=17
    u8 y = RAND_RANGE(3, 5);
    ret |= y << 8;
    ret |= RAND_RANGE(y + 1, 17);

    return (void *)ret;
}

int main(i32 argc __attribute__ ((unused)), char *argv[] __attribute__ ((unused)))
{
    u8 success = 1;
    i32 server_socket = -1, client_socket = -1;
    struct sockaddr_in server_addr = {0}, client_addr = {0};

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(LISTEN_PORT);

    // Open listen port
    server_socket = socket(AF_INET, SOCK_STREAM, 0);
    success = server_socket >= 0;

    // Allow re-use
    if (success)
    {
        success = setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) >= 0;
    }

    // Bind
    if (success)
    {
        success = bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) >= 0;
    }

    // Listen
    if (success)
    {
        success = listen(server_socket, 0) == 0;
    }

    // Loop for client
    while (success)
    {
        socklen_t client_size = sizeof(client_addr);
        client_socket = accept(server_socket, (struct sockaddr *) &client_addr, &client_size);
        success = client_socket >= 0;

        // Fork client
        if (success)
        {
            pid_t pid = fork();

            // Error
            if (pid < 0)
            {
                success = 0;
            }

            // Client
            else if (pid == 0)
            {
                // Close server socket
                close(server_socket);
                server_socket = -1;

                // Get client locations
                void *handle = dlopen("libblock_dude.so", RTLD_NOW);
                if (NULL == handle)
                {
                    fprintf(stderr, "dlopen: %s\n", dlerror());
                    exit(EXIT_FAILURE);
                }

                void *client_game = dlsym(handle, "client_game");
                //void *debug_flag = dlsym(handle, "debug_flag");
                void *get_block_at = dlsym(handle, "get_block_at");
                assert((get_block_at - client_game) < 0x1000);

                // Place client in memory
                void *addr = rand_address();
                void *ret_offset = (void *)0x28;
                void *aligned = (void *)(((u32)addr) & ~0xFFFul);
                void *ptr = mmap(aligned, 0x2000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
                if ((void *)0xFFFFFFFFFFFFFFFF == ptr || ptr != aligned)
                {
                    fprintf(stderr, "ptr: %p\n", ptr);
                    perror("mmap");
                    exit(EXIT_FAILURE);
                }

                // Copy code into memory
                void (*trampoline)(i32 socket) = (void (*)(i32 socket))(addr - ret_offset);
                memcpy(trampoline, client_game, get_block_at - client_game);

                // Patch calls - super hacky but should work in this limited scope
                for (u32 i = 0; i < (u32)(get_block_at - client_game); i++)
                {
                    // Found call instruction
                    if (0xe8 == ((u8 *)trampoline)[i])
                    {
                        void *offset = *(void **)(((u8 *)trampoline) + i + 1);
                        offset += client_game - (void *)trampoline;
                        *((void **)(((u8 *)trampoline) + i + 1)) = offset;
                        i += 4;
                    }
                }

                // Make executable
                assert(0 == mprotect(aligned, 0x2000, PROT_READ | PROT_EXEC));

                // Call client handler
                (void)trampoline(client_socket);
                send(client_socket, "Winner Winner Chicken Dinner!", sizeof("Winner Winner Chicken Dinner!"), 0);
                close(client_socket);
                exit(0);
            }

            // Server
            else
            {
                // Close client socket
                close(client_socket);
                client_socket = -1;
            }
        }
    }

    if (server_socket >= 0)
    {
        close(server_socket);
    }
}
