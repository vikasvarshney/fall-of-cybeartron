import os.path
import re
import requests
import requests.compat

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class CTFdAuthenticatedAPISession(requests.Session):
    def __init__(self, ctfd_uri, passphrase=None):
        super(CTFdAuthenticatedAPISession, self).__init__()
        # This is used to anchor all requests to the v1 API
        self._api_url = "{}/api/v1/".format(ctfd_uri)
        # We authenticate with the /auth endpoint and then stash the CSRF token
        # in our headers so it's used for all requests
        login_url = "{}/login".format(ctfd_uri)
        login_get_r = super(CTFdAuthenticatedAPISession,self).get(login_url)
        assert login_get_r.status_code == 200
        if login_get_r.status_code != 200:
            raise Exception(
                "Unable to reach the CTFd login page: %r" % login_get_r.content
            )
        # If we we're given a passphrase then we can try to load the default
        # file adjacent to this script
        with open(
            os.path.join(SCRIPT_DIR, ".ctfd_admin_pass"), "r"
        ) as passfile:
            passphrase = passfile.read()
        login_post_r = super(CTFdAuthenticatedAPISession, self).post(
            login_url, data={
                "name": "cybears",
                "password": passphrase,
                "nonce": self.extract_csrf_nonce(login_get_r.content),
            }
        )
        if login_post_r.status_code != 200:
            raise Exception(
                "Unable to authenticate with CTFd: %r" % login_post_r.content
            )
        # Stash the CSRF token for later requests
        self.headers["CSRF-Token"] = self.extract_csrf_nonce(
            login_post_r.content
        )

    @staticmethod
    def extract_csrf_nonce(response_data):
        return re.search(
            rb'csrf_nonce = "([0-9a-f]{64})"', response_data
        ).group(1)

    @property
    def nonce(self):
        # Sometimes we need to pass a nonce using form encoding so we make it
        # available as a read only property
        return self.headers.get("CSRF-Token")

    def request(self, method, url, *args, **kwargs):
        url = requests.compat.urljoin(self._api_url, url)
        return super(CTFdAuthenticatedAPISession, self).request(
            method, url, *args, **kwargs
        )
