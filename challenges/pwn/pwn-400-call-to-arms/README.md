pwn-400-call-to-arms
=================

## Flags
* cybears{fun_with_uaf_and_vtables}

## Challenge Text
This is set toward the end of the story - and the word "use" after the word "free" in the challenge text wasn't an accident. :)

```markdown
Now that the Cybears and DecepticomTSS are free from their conflict it is time to use their newfound truce to prepare for what is to come.

The attached Dockerfile should make running this challenge easier
```

## Attachments
* handout/Dockerfile
* handout/call-to-arms

## References
* https://0x00sec.org/t/heap-exploitation-abusing-use-after-free/3580

## Notes
* The Dockerfile is provided for 2 reasons, the first is what is explained in the challenge text but the second and main reason for it is that it will be using the same shared libraries that we will be (which is important for building the exploit)
