# k3s

k3s is an easy to deploy kubernetes environment for testing the CTF.

To get started you'll need:
- docker
- [docker-compose](https://pypi.org/project/docker-compose/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

```bash
# Start k3s
docker-compose up -d
# Make sure you're using the config for k3s
export KUBECONFIG="`readlink -f config/kubeconfig.yaml`"
# Use kubernetes!
kubectl get nodes
```
