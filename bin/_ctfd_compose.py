import os.path
import subprocess

try:
    subprocess.check_call(["docker", "info"], stdout=subprocess.DEVNULL)
except subprocess.CalledProcessError as exc:
    print("docker info failed - will use sudo")
    DOCKER_COMPOSE_CMD = ("sudo", "docker-compose", )
else:
    DOCKER_COMPOSE_CMD = ("docker-compose", )

def start_ctfd(compose_path):
    subprocess.check_call(
        DOCKER_COMPOSE_CMD +  ("up", "--build", "--detach"),
        cwd=os.path.join(compose_path)
    )
    # We know that CTFd want to listen on port 8000 in its container so we can
    # ask `docker-compose` what that's been mapped to externally
    ctfd_host = subprocess.check_output(
        DOCKER_COMPOSE_CMD + ("port", "ctfd", "8000"),
        cwd=os.path.join(compose_path)
    ).decode().strip()
    # We probably bound on all IPs so just use a 127 address
    ctfd_host = ctfd_host.replace("0.0.0.0", "127.0.0.1")
    return "http://{}/".format(ctfd_host)

def stop_ctfd(compose_path):
    # Stop CTFd using docker-compose - the containers and whatnot will remain
    subprocess.check_call(
        DOCKER_COMPOSE_CMD +  ("stop", ),
        cwd=os.path.join(compose_path)
    )

def purge_ctfd(compose_path):
    # Purge CTFd using docker-compose - this will remove named and anonymous
    # volumes as well to destroy all state
    subprocess.check_call(
        DOCKER_COMPOSE_CMD +  ("down", "--volumes", ),
        cwd=os.path.join(compose_path)
    )
