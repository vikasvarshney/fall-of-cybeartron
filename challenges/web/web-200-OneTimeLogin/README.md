web-200-OneTimeLogin
=================

* _title_: A Stitch in Time
* _points_: 200
* _tags_: web

## Flags
* `cybears{s33ds_4r3_s3cr3t5}`


## Challenge Text
```markdown
The DecepticomTSS forces have some kind of 2FA enabled on their web application login. Can you find a way around it?

Head to http://astitchintime.cybears.io

```

## Attachments
*

## Hints
*

## References

## Notes
* Build locally with `sudo docker build -t astitchintime .`
* Run locally with `sudo docker run -it -p 8080:80 --name astitchintime_test -td  astitchintime:latest`
* Browse to `http://localhost:8080` to hit login screen

<details>
<summary>SPOILERS - Walkthrough</summary>
<p>

## Walkthrough
* Users see a login screen and QR code generator for google authenticator
* Attempt to log in multiple times and receive log in error flash message
* Looking at web browser log also shows that errors are written to log file
* Attempt to browse to either /log or /logs (both work), find directory listing for error.log
* Browse log and find time stamps and admin password (`admin:Cyb34rsRoll0ut!`)
* Read the js to realise that this password is also reused at OTP secret key
* use built in js functions to generate OTP code with password/secret key
* This should fail - uesr should go back to the logs and find out that the server isn't set to AEDT and they'll need to adjust the time that they are requesting the OTP PIN for to accomodate
* This javascript, run in console should generate the correct OTP to login with the above creds (if entered in a reasonable timeframe)

```javascript
password = 'Cyb34rsRoll0ut!';
key = password;
key = base32.encode(key);
key = key.replace(/=/g, ""); //Strip out the = padding if required
console.log('Key: ' + key);
var keyHex = base32tohex(key);
keyHex = keyHex.replace(/-/g, ""); //bug in base32tohex which sometimes returns -ve values
console.log('Key: ' + keyHex);
genOTP(keyHex, Math.round((new Date().getTime())/ 1000.0 +60*60*7+87 ));
```

</p>
</details>
