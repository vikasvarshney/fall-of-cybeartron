# Easy Strings

* title: Easy Strings
* difficulty: easy
* tags: re

## Skills Required

- Basic x86 reverse engineering
   - Debuggers, binary patching or x86 assembly

## Solution

This binary contains a method that isn't called, but contains a stack built string.
It directly calls the write syscall to make the flag non-obvious.

Players can patch the binary, reverse the method or use a debugger to jump to the
function.

My CI script is the harder of these solutions, it disassembles the binary and
rebuilds the string as a player would if they chose the reverse engineering
method.

## Flag

```
cybears{st4ck_Bu1lt}
```
