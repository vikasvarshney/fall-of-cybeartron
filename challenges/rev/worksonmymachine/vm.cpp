/**
 * @brief Works On My Machine VM
 **/
#include "worksonmymachine.h"
#include "program.h"
#include <stdint.h>
#include <stdio.h>
#include <windows.h>

#define NUMBER_OF_REGISTERS    8
#define STACK_SIZE            64

typedef struct MyMachine_t {
    uint32_t registers[NUMBER_OF_REGISTERS];
    uint32_t ip;
    uint8_t exit_flag;
    uint8_t zero_flag;
    uint32_t sp;
    uint32_t stack[STACK_SIZE];
} MyMachine;

// Machine extension API
// CanDo is called to check if the DLL can handle an instruction
typedef bool(__cdecl *CanDo_t)(const uint8_t instruction);
// Do is called to handle an instruction
typedef bool(__cdecl *Do_t)(const uint8_t instruction, const uint8_t * program, MyMachine * machine_state, char * output_buffer, size_t output_buffer_size);


// Virtual machine implementation

// MSVC and GCC have different ways of specifying "place this code in this segment"
// Use a function-level attribute for GCC
#ifdef _MSC_BUILD
#define MINGW_ATTRIBUTE_CTF_SECTION
#else
#define MINGW_ATTRIBUTE_CTF_SECTION        __attribute__((section(".ctforig")))
#endif
// and use pragmas for MSVC
#ifdef _MSC_BUILD
#pragma code_seg(push, r1, ".ctforig")
#endif

// VM implementation
uint32_t __declspec(noinline) MINGW_ATTRIBUTE_CTF_SECTION GenerateFlag(char * output_buffer, size_t output_buffer_size)
{
    MyMachine machine = { 0 };
    HMODULE extension_library = NULL;

    // Reset VM state
    machine.sp = STACK_SIZE;

    while (!machine.exit_flag)
    {
        CTF_PRINTF("0x%x: ", machine.ip);
        uint8_t this_instruction = g_Program[machine.ip++];
        bool handled = false;
        switch (this_instruction)
        {
        case EXIT:
            CTF_PRINTF("EXIT\n");
            machine.exit_flag = 1;
            handled = true;
            break;
        case PUSH_U32:
        {
            uint32_t value = *(uint32_t*)(g_Program + machine.ip);

#ifdef CTF_DEBUG
            char value_chars[5] = { 0 };
            memcpy(value_chars, &value, 4);
            CTF_PRINTF("PUSH 0x%x (%s)\n", value, value_chars);
#endif
            machine.ip += 4;
            machine.sp--;
            machine.stack[machine.sp] = value;
            handled = true;
        }
        break;
        case PUSH_REG:
        {
            uint8_t reg = g_Program[machine.ip++];
            CTF_PRINTF("PUSH R%d\n", reg);
            machine.sp--;
            machine.stack[machine.sp] = machine.registers[reg];
            handled = true;
        }
        break;
        case POP_REG:
        {
            uint8_t reg = g_Program[machine.ip++];
            CTF_PRINTF("POP R%d\n", reg);
            machine.registers[reg] = machine.stack[machine.sp++];
            handled = true;
        }
        break;
        case XOR_REG_REG:
        {
            uint8_t reg1 = g_Program[machine.ip++];
            uint8_t reg2 = g_Program[machine.ip++];
            CTF_PRINTF("XOR R%d = R%d ^ R%d\n", reg1, reg1, reg2);
            machine.registers[reg1] = machine.registers[reg1] ^ machine.registers[reg2];
            handled = true;
        }
        break;
        case MOVD_RESULT_OFFSET_REG:
        {
            uint8_t dest_offset = g_Program[machine.ip++];
            uint8_t src_reg = g_Program[machine.ip++];
            volatile uint32_t dest_value = 0;
            CTF_PRINTF("MOVD result[%d] = R%d\n", dest_offset, src_reg);
#ifndef CTF_DEBUG
            // Release
            printf("oops, instruction move dword result[X] = register Y not implemented yet!\n");

            dest_value = 0x206c6f6c;

#ifdef _MSC_BUILD
            __asm
            {
                nop;
                nop;
                nop;
                nop;
                nop;
            }
#else
            asm("nop");
            asm("nop");
            asm("nop");
            asm("nop");
            asm("nop");
#endif // _MSC_BUILD

#else
            // Debug
            dest_value = machine.registers[src_reg];
#endif // CTF_DEBUG
            *(uint32_t*)(output_buffer + dest_offset) = dest_value;
            // we don't set handled=true so that we fall through to the LoadLibrary call below
        }
        break;
        case MOVD_REG_RESULT_OFFSET:
        {
            uint8_t dest_reg = g_Program[machine.ip++];
            uint8_t src_offset = g_Program[machine.ip++];
            CTF_PRINTF("MOVD R%d = result[%d]\n", dest_reg, src_offset);
            machine.registers[dest_reg] = *(uint32_t*)(output_buffer + src_offset);
            handled = true;
        }
        break;
        default:
            //printf("ERROR: Invalid instruction: 0x%x\n", this_instruction);
            break;
        }


        if (!handled)
        {
            // Try loading a DLL to implement this instruction instead
            if (NULL == extension_library)
            {
                extension_library = LoadLibraryA("VMExtensions.dll");
            }
            if (NULL != extension_library)
            {
                CanDo_t can_do = (CanDo_t)GetProcAddress(extension_library, "CanDo");
                Do_t do_instruction = (Do_t)GetProcAddress(extension_library, "Do");

                if (NULL != can_do && NULL != do_instruction)
                {
                    if (can_do(this_instruction))
                    {
                        if (do_instruction(this_instruction, g_Program, &machine, output_buffer, output_buffer_size))
                        {
                            // good
                        }
                        else
                        {
                            printf("ERROR: Extension DLL failed to handle instruction type 0x%x\n", this_instruction);
                        }
                    }
                    else
                    {
                        printf("ERROR: Extension DLL cannot handle instruction type 0x%x\n", this_instruction);
                    }
                }
                else
                {
                    printf("ERROR: Extension DLL missing some imports\n");
                }
            }
        }
    }

    if (NULL == extension_library)
        FreeLibrary(extension_library);

    return machine.registers[0];
}

#ifdef _MSC_BUILD
#pragma code_seg(pop, r1)
#endif