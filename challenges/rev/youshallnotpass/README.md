rev-200-YouShallNotPass
=================

* _title_: You Shall Not Pass!
* _points_: 200
* _tags_: rev,crypto

## Flags
* `cybears{RU_SAT-15f13d?}`

## Challenge Text
```markdown
We've managed to find the DecepticomTSS app that looks like it's still in development. We just need to find the password to get in...

Flag format is "cybears{flagflagflag}"

```

## Attachments
* PasswordChecker.apk

## Hints

## References
* https://ctftime.org/writeup/9875
* https://fortenf.org/e/ctfs/re/2017/09/18/csaw17-realism.html

## Notes
* Users will need to decompile the APK and realise that the password check function is in a native ARM binary
* The binary will be doing a long series of validation on the input string
* Users will know the length from RE (or be able to do s small brute force to check it)
* They will need to use a SAT/constraint solver like z3 to find a string that satisfies all of the equations. These rules, along with the known start and end of flag format should recover the flag uniquely

* The file `SAT_challenge_generator.py` will take a flag and generate two files, `password.c` (a basic C-program which takes a password from the cmd line and validates it for inclusion into other packages) and `password_solve.py` which will take the constraints/equations (found from RE) and solves for the flag, confirming uniqueness. There is an example password.c and password_solve.py in the repo.
* Compile with `gcc password.c`
* Solve with `python password_solve.py` #requires python2 and z3, z3-solver packages

* Note in this password.c/password_solve.py, you can actually remove all equations after `x9 - x16 - x14 - x3 == -114` (leaving 22 equations) and it will still uniquely solve. This might be better than getting them to RE 46 equations...
* The Android Studio project is included under PasswordChecker/ it contains all the source to build the apk. It's current build makes a 'universal' apk which includes x86, x86_64, arm32 and arm64 binary libraries. We had a discussion about whether to just include the arm libraries, but thought if people use ida free, it only has x86_64 so we'd leave it in. It is also signed and built for Android 9 - have confirmed installed and works on a Pixel3.
