# XXX: It would be better to use the `kubernetes` package for this
import os.path
import subprocess
import yaml

# Define a default kubectl to use and allow other modules to override it
KUBECTL_PROG = ("kubectl", )
# We use these if we want to be quiet while running subprocesses (the default)
QUIET_KWARGS = {
    "stdout": subprocess.DEVNULL,
    "stderr": subprocess.DEVNULL,
}

def create(deploy_yml_path, can_fail=False, quiet=True):
    try:
        # Attempt to create by default
        subprocess.check_call(
            KUBECTL_PROG + ("create", "-f", deploy_yml_path, ),
            **QUIET_KWARGS if quiet else {}
        )
    except subprocess.CalledProcessError as exc:
        if not can_fail:
            raise exc

def create_or_replace(deploy_yml_path, quiet=True):
    try:
        # Attempt to create by default
        create(deploy_yml_path, can_fail=False, quiet=quiet)
    except subprocess.CalledProcessError:
        # And replace if creation failed, potentially because it already exists
        subprocess.check_call(
            KUBECTL_PROG + ("replace", "-f", deploy_yml_path, ),
            **QUIET_KWARGS if quiet else {}
        )

def delete(deploy_yml_path, quiet=True):
    try:
        # Attempt to create by default
        subprocess.check_call(
            KUBECTL_PROG + ("delete", "-f", deploy_yml_path, ),
            **QUIET_KWARGS if quiet else {}
        )
    except subprocess.CalledProcessError:
        pass

def wait_for_deployment(name, ns="default", quiet=True):
    subprocess.check_call(
        KUBECTL_PROG + (
            "rollout", "status", "--namespace", ns,
            "deployment/{}".format(name)
        ), **QUIET_KWARGS if quiet else {}
    )

def get_service_address(name, ns="default"):
    service_yml = subprocess.check_output(
        KUBECTL_PROG + (
            "get", "services", "--output", "yaml", "--namespace", ns, name
        )
    )
    service_info = yaml.load(service_yml)
    service_ingress = service_info["status"]["loadBalancer"]["ingress"][0]
    if "hostname" in service_ingress:
        addr = service_ingress["hostname"]
    else:
        addr = service_ingress["ip"]
    # We use the `nodePort` if it's present since that's what's actually
    # available on budget load balancers like klipper which is used by k3s
    try:
        port = service_info["spec"]["ports"][0]["nodePort"]
    except KeyError:
        port = service_info["spec"]["ports"][0]["port"]
    if port not in (80, 443):
        addr += ":{}".format(service_info["spec"]["ports"][0]["port"])
    return addr

def find_or_prompt_docker_secret(name, ns="default"):
    try:
        subprocess.check_call(
            KUBECTL_PROG + ("get", "secrets", "--namespace", ns, name),
            **QUIET_KWARGS
        )
    except subprocess.CalledProcessError:
        # It must not have existed - run the `add_k8s_secret.sh` script
        # XXX: We should do this ourselves, calling a shell script is dumb
        secret_script = os.path.normpath(os.path.join(
            os.path.dirname(__file__), "add_k8s_secret.sh"
        ))
        subprocess.check_call((secret_script, name, ns, ))
