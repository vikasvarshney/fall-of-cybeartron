import distutils.util
import os.path
import random
import subprocess
import sys
import tempfile
import yaml

import _k8s
from _k8s import (
    create, create_or_replace, delete,
    wait_for_deployment, get_service_address,
    find_or_prompt_docker_secret
)

# Use the right input on Python 2
try:
    input = raw_input
except NameError:
    pass

# XXX: This is a k8s YAML from the internet so we should warn the user before
# trying to use it!
LOCAL_PATH_STORAGECLASS_YAML = (
    "https://raw.githubusercontent.com/rancher/local-path-provisioner"
    "/master/deploy/local-path-storage.yaml"
)

# Work out if we can use `k3s kubectl` or if we'll fall back to `kubectl`
try:
    subprocess.check_call(("k3s", ), stdout=subprocess.DEVNULL)
except FileNotFoundError:
    # Just use `kubectl` and hope the user isn't lying to us and making us do a
    # bunch of workarounds that probably aren't necessary on other platforms
    K3S_KUBECTL_PROG = ("kubectl", )
else:
    K3S_KUBECTL_PROG = ("k3s", "kubectl", )

def prompt(query):
   sys.stdout.write('%s [y/n]: ' % query)
   val = input()
   try:
       ret = distutils.util.strtobool(val)
   except ValueError:
       sys.stdout.write('Please answer with a y/n\n')
       return prompt(query)
   return ret

def rollout_local_path_storage():
    if not prompt("Can I use a YAML from the net for local-path storage?"):
        try:
            wait_for_deployment(
                "local-path-provisioner", ns="local-path-storage"
            )
        except subprocess.CalledProcessError as exc:
            raise Exception("Okay I won't use it then") from exc
        else:
            print("You already have one running, I'll use that")
            return
    print("This will pollute your system at /opt/local-path-provisioner")
    create_or_replace(LOCAL_PATH_STORAGECLASS_YAML)
    print("local-path storage class system is being deployed")
    wait_for_deployment("local-path-provisioner", ns="local-path-storage")

def start_ctfd(deployment_dir):
    # Tell the k8s module that it'll be using k3s as an intermediary
    _k8s.KUBECTL_PROG = K3S_KUBECTL_PROG
    # We have to spin up a `local-path` storage class to make persistent volume
    # claims work in k3s
    rollout_local_path_storage()
    # Do the actual deployment after applying some fixups to the `deploy.yml`
    with tempfile.NamedTemporaryFile(mode="w", delete=False) as mod_yml:
        # We have to modify the CTFd deployment.yml to make it work in k3s
        with open(os.path.join(deployment_dir, "deploy.yml")) as orig_yml:
            deployment_docs = tuple(yaml.load_all(orig_yml.read()))
        for doc in deployment_docs:
            # We need PVCs to use the local-path storage class since k3s has no
            # default which it'll use
            if doc["kind"] == "PersistentVolumeClaim":
                doc["spec"]["storageClassName"] = "local-path"
            # We need load balancers to not try to bind port 80 or 443 because
            # k3s' kubes services listen on those ports
            if (
                doc["kind"] == "Service" and
                doc["spec"]["type"] == "LoadBalancer"
            ):
                for port_entry in doc["spec"]["ports"]:
                    if port_entry["port"] in (80, 443):
                        # Pick a random IANA ephemeral port - good enough
                        port_entry["port"] = random.randrange(
                            2**15 + 2**14, 2**16 - 1
                        )
        # Write the mutated YAML out to the tempfile
        dumped_yml = yaml.dump_all(deployment_docs)
        mod_yml.write(dumped_yml)
        mod_yml.flush()
        # Now we can create the CTFd deployment using the modified deployment.
        # We can't replace certains parts of the CTFd deployment so we might
        # fail and should just move on to see if we can discover the service.
        create(mod_yml.name, can_fail=True)
    # We need to add a secret to the ctfd namespace - this prompts the user
    find_or_prompt_docker_secret("regcred", ns="ctfd")
    # We should be able to wait for the rollout now
    wait_for_deployment("ctfd-cache", ns="ctfd")
    wait_for_deployment("ctfd-db", ns="ctfd")
    wait_for_deployment("ctfd-web", ns="ctfd")
    # And now pull the external service address - we always use HTTPS when we
    # deploy on kubernetes thanks to the nginx sidecar container
    return "https://{}/".format(get_service_address("ctfd-web", ns="ctfd"))

def stop_ctfd(deployment_dir):
    # XXX: Need to work out how to take it down and then back up...
    purge_ctfd(deployment_dir)

def purge_ctfd(deployment_dir):
    # Tell the k8s module that it'll be using k3s as an intermediary
    _k8s.KUBECTL_PROG = K3S_KUBECTL_PROG
    delete(os.path.join(deployment_dir, "deploy.yml"), quiet=False)
