import io
import os
import subprocess
import tempfile
import uncompyle6

EXPECTED_FLAG = b"cybears{l3t5_c@u53_s0m3_m@yh3m!}"

source = io.StringIO()
uncompyle6.decompile_file("./handout/ransom.pyc", outstream=source)
source.seek(0)
(pubkey_line, ) = (l for l in source.readlines() if "BEGIN PUBLIC" in l)
# Hope it's not evil ;)
eval_dict = dict()
exec(pubkey_line.strip(), eval_dict, eval_dict)
pubkey = eval_dict["public_key_str"]
print("[-] Extracted public key\n%s" % pubkey)

RSACTFTOOL_PATH = os.environ["RSACTFTOOL_PATH"]
with tempfile.NamedTemporaryFile(mode="w") as f:
    f.write(pubkey)
    f.flush()
    privkey = subprocess.check_output((
        "python3", os.path.join(RSACTFTOOL_PATH, "RsaCtfTool.py"),
        "--publickey", f.name, "--attack", "fermat", "--private"
    ))
print("[-] Recovered private key\n%s" % privkey.decode())

with tempfile.NamedTemporaryFile(mode="wb") as f:
    f.write(privkey)
    f.flush()
    # Creates ThreatIntellReport.pdf.enc.dec
    subprocess.check_call((
        "python3", "./handout/ransom.pyc",
        "-i", "./handout/ThreatIntellReport.pdf.enc",
        "-m", "decrypt", "-v", f.name
    ))
print("[-] Decrypted PDF document")

output = subprocess.check_output((
    "pdf2txt.py", "./handout/ThreatIntellReport.pdf.enc.dec",
))
print("[-] Extracted PDF text lines\n%s" % output.decode())
assert EXPECTED_FLAG in output
print("SUCCESS! Test passed")
