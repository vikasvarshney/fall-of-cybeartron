cmake_minimum_required (VERSION 3.5)
include(GoogleTest OPTIONAL)
project (qarma)

add_library(qarma_lib qarma.c)

add_executable(qarma main.c)

set(qarma PROPERTIES
    POSITION_INDEPENDENT_CODE ON)

target_link_libraries(qarma qarma_lib)

# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in ext/googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ext/googletest-download )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ext/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/ext/googletest-src
                 ${CMAKE_CURRENT_BINARY_DIR}/ext/googletest-build
                 EXCLUDE_FROM_ALL)


if(COMMAND gtest_add_tests)
  # no tests for you cmake 3.5 on ubuntu 16.04
  include_directories("${gtest_SOURCE_DIR}/include")
  add_executable(qarmaTest qarmaTest.cpp)
  target_link_libraries(qarmaTest gtest_main)
  target_link_libraries(qarmaTest qarma_lib)
  gtest_add_tests(qarmaTest "" AUTO)
endif()
