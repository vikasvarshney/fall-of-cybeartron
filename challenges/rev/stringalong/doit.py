#!/usr/bin/env python

'''
To run this you'll need the r2pipe library:
pip install r2pipe
'''

import unittest
import r2pipe
import json

class StringAlongTest(unittest.TestCase):
    def test_stack_string(self):
        r = r2pipe.open("handout/stringalong")
        # Make sure we're running on intel asm syntax for mnemonic matching
        r.cmd('e asm.syntax=intel')
        r.cmd('aaa') # Analyze the binary

        # Ensure a simple strings won't find the flag
        strings = json.loads(r.cmd('izj')) # list all the strings
        for s in strings:
            self.assertFalse('cybears{' in s['string'], 'Strings detected the flag')

        # Find the function containing the stack built string
        symbols = json.loads(r.cmd('isj')) # list the symbols
        print_func = 0
        for s in symbols:
            if s['name'] == 'print_flag' and s['type'] == 'FUNC':
                print_func = s['vaddr']
                print("Found print_flag address at : %x" % (print_func))
        self.assertNotEqual(0, print_func, "Failed to find the print function 'print_flag'. Maybe the optimizer got it?")

        # Disassemble and reconstruct the stack built string
        r.cmd('s 0x%x' % (print_func))
        instructions = json.loads(r.cmd('pdfj')) # Disassemble the print_flag function
        stack_str = ''
        for i in instructions['ops']:
            if 'mov byte' in i['opcode']:
                stack_str += chr(i['val'])

        # Ensure we extracted the whole thing.
        print(stack_str)
        self.assertEqual('cybears{st4ck_Bu1lt}', stack_str, 'The stack built flag string is incorrect!')

if __name__ == "__main__":
    unittest.main()
