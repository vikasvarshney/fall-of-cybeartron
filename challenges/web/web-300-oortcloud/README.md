web-300-oortcloud
=================

* _author_: Cybears:cipher
* _title_: Oort Cloud
* _points_: 300
* _tags_: web, crypto

## Flags
* `cybears{forward secrecy? where we're going, we don't need forward secrecy}`

## Challenge Text
```markdown
CyberBook is the hottest new social network in the galaxy and one of the only places that Cybears and Decepticoms come together. Sign-up now!

https://oortcloud.cybears.io

```

## Attachments
* None

## Hints
* 

## References
* `https://cloudshark.io/articles/examples-of-tls-1-3/`
* `https://www.blackhat.com/us-18/briefings/schedule/#playback-a-tls-13-story-10192`
* `https://tls13.ulfheim.net/`

## Notes
* This challenges specifically targets the new feature in TLS1.3 of "Zero round Trip Time" or 0-RTT handshakes. It is similar to facebooks zero protocol which allows pre-established secure sessions to re-establish a connection in a single trip, without renegotiating a new session ticket/authenticating. This is also called "early data" 
* 0-RTT/early data has lesser security properties than a full TLS 1.3 session, in particular, it does not guarantee forward secrecy and is susceptible to replays. Because of this, in the RFC, there are a lot of caveats about how and where to enable 0-RTT. For web servers, the suggestion is to only allow this for idempotent (non-state changing) requests. 
* In this challenge, TLS1.3 + early data is enabled on the HTTPS reverse proxy, and there is a flask web app sitting behind it
* The web app is a "social network" and allows people to give and receive energon/likes. You need to have at least 10 energon to give energon, and to see the flag
* There is a user on another server that will automatically "give energon" to new users with TLS1.3 early data. The players will need to replay this. 
* There is a tcpdump process running which is dropping TLS packets received to an accessible location. 

## Deploy Notes
* There are two docker containers communicating on a docker network. The server component (teletraan1) has an nginx reverse proxy, a flask web app and a tcpdump process (see diagram)
* The client component (moonbase1) has a python script and custom openssl build to poll the server looking for new users to "give energon"

[diagram of server setup]

* Create the network
`sudo docker network create --driver bridge cybearsnet`
* build the two containers
`sudo docker build -f Dockerfile-teletraan-1-no_geoip -t teletraan1 .`
`sudo docker build -f Dockerfile-moonbase-1 -t moonbase1 .`
* (Note I had some issues with the geoip module in nginx - we don't need it, Dockerfile-teletraan-1-no_geoip should work instead)
* run the two containers joined to the same network 
* `sudo docker run -it -p 8080:80 -p 8443:443 --name teletraan1 -td --network cybearsnet teletraan1:latest`
`sudo docker run -it --name moonbase1 -td --network cybearsnet moonbase1 `

* From your host, you can now connect to `https://localhost:8443` to get to cyberbook application or `http://localhost:8080` to get to the nginx default page

### Deployment on kubernetes:
As a note for people trying to replicate the deployment we used for the BSides
CTF, we had some trouble with the packet captures not working in a single pod
environment on k8s. We ended up just deploying it on a standalone machine using
the docker-only approach described above. We expect that the challenge would
work if we split up the k8s deployment into 2 pods and captured traffic leaving
the client pod but never ended up getting that done.

## Solve notes/walk through
* Player browses to cyberbook, registers and browses
* Sees different users and posts, attempts to give energon to others and themselves - realises you need at least 10 energon to give energon
* Reads posts - may get hints that there is a user `alpha_trion` that will give energon to all users. May notice after a minute or two that their registered user went from zero to two energon
* Tries to get flag page - realises you need at least 10 energon to get flag
* Looks at source for pages - finds a hint that captures are stored 
* Finds the pcap logs at `http://localhost:8080/pcap`
* analyses the pcaps - finds that `alpha_trion` is connecting via TLS1.3
* reads a lot about TLS1.3 and realises that `alpha_trion` is sending `give_energon` with early data/0-rtt
* re-registers a new user or finds (in time) the pcap where `alpha_trion` updates their user
* identifies the early-data (it will be the only handshake that has a Client Hello, Change Cipher Spec and Application data in the single packet. The wireshark filter `ssl.handshake.type == 1` will filter on Client Hello
* Sends this (replays this) to the server (8-times)
* logs back into the website and receives flag


