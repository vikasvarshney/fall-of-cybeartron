
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <endian.h>
#include "qarma.h"
#include "qarmaLib.h"

inline void xorBlock( uint8_t *a, uint8_t *b, uint8_t *out, uint8_t size) {
    for(int i = 0; i < size; i++) {
        out[i] = a[i] ^ b[i];
    }
}
inline void copy_matrix(uint8_t *in, uint8_t *out) {
    ((QarmaMatrix_t*)out)->llrow = ((QarmaMatrix_t*)in)->llrow;
}

inline void permuteTweek(uint8_t *tweakIn, uint8_t *out, uint8_t size) {
    copy_matrix(tweakIn, out);
    QarmaMatrix_t *tweak = (QarmaMatrix_t*)out;
    /* h = [6, 5, 14, 15, 0, 1, 2, 3, 7, 12, 13, 4, 8, 9, 10, 11] */
    uint16_t row1 = tweak->row[1];
    uint16_t row3 = tweak->row[3];
    tweak->row[1] = tweak->row[0];
    tweak->row[3] = tweak->row[2];
    tweak->row[0] = ((row1 >>  8) & 0x00F0U) |
                    (row1        & 0x000FU) |
                    (row3        & 0xFF00U);
    tweak->row[2] = ((row1 <<  4) & 0x0F00U) |
                    ((row1 >>  4) & 0x00F0U) |
                    ((row3 >>  4) & 0x000FU) |
                    ((row3 << 12) & 0xF000U);
}

static inline void lfsr_tweak_cell(uint8_t *cell) {
    uint8_t b0 = *cell & 1;
    uint8_t b1 = (*cell >> 1) & 1;
    uint8_t b2 = (*cell >> 2) & 1;
    uint8_t b3 = (*cell >> 3) & 1;
    *cell = ((b0 + b1) << 3) | (b3 << 2) | (b2 << 1) | b1;
}

static inline uint8_t get_cell(QarmaMatrix_t *inMatrix, uint8_t index) {
    uint8_t row_index = index / 4;
    uint8_t col_index = index % 4;
    uint16_t *row = &inMatrix->row[row_index];
    uint8_t cell = 0;
    switch(col_index) {
        case 0:
            cell = ((uint8_t*)row)[0] & 0xF;
            break;
        case 1:
            cell = ((uint8_t*)row)[0] >> 4;
            break;
        case 2:
            cell = ((uint8_t*)row)[1] & 0xF;
            break;
        case 3:
            cell = ((uint8_t*)row)[1] >> 4;
            break;
        default:
            break;
    }
    return cell;
}

static inline void set_cell(QarmaMatrix_t *inMatrix, uint8_t index, uint8_t val) {
    uint8_t row_index = index / 4;
    uint8_t col_index = index % 4;
    uint16_t *row = &inMatrix->row[row_index];
    switch(col_index) {
        case 0:
            ((uint8_t*)row)[0] = (((uint8_t*)row)[0] & 0xF0) | (val & 0xF);
            break;
        case 1:
            ((uint8_t*)row)[0] = (((uint8_t*)row)[0] & 0xF) | (val & 0xF) << 4;
            break;
        case 2:
            ((uint8_t*)row)[1] = (((uint8_t*)row)[1] & 0xF0) | (val & 0xF);
            break;
        case 3:
            ((uint8_t*)row)[1] = (((uint8_t*)row)[1] & 0xF) | (val & 0xF) << 4;
            break;
        default:
            break;
    }
}

inline void permuteState( uint8_t *state, uint8_t *out, bool inverse, uint8_t size) {

    QarmaMatrix_t temp;
    uint8_t tempi;
    for(int i = 0; i < sizeof(statePermutation); i++) {
        uint8_t index = inverse ? statePermutation_inv[i] : statePermutation[i];
        tempi = get_cell((QarmaMatrix_t*)state, index);
        set_cell(&temp, i, tempi);
    }
    copy_matrix((uint8_t *)&temp, out);
}

inline void mixColums(QarmaMatrix_t *inMatrix) {
    for(int i = 0; i < 4; i++) {
        uint8_t r0, r1, r2, r3, r4;
        uint8_t n0, n1, n2, n3, n4;
        r0 = get_cell(inMatrix, 0 + i);
        r1 = get_cell(inMatrix, 4 + i);
        r2 = get_cell(inMatrix, 8 + i);
        r3 = get_cell(inMatrix, 12 + i);

        n0 = rot8(r1, 1) ^ rot8(r2, 2) ^ rot8(r3,1);
        n1 = rot8(r0, 1) ^ rot8(r2, 1) ^ rot8(r3,2);
        n2 = rot8(r0, 2) ^ rot8(r1, 1) ^ rot8(r3,1);
        n3 = rot8(r0, 1) ^ rot8(r1, 2) ^ rot8(r2,1);
        set_cell(inMatrix, i, n0);
        set_cell(inMatrix, 4 + i, n1);
        set_cell(inMatrix, 8 + i, n2);
        set_cell(inMatrix, 12 + i, n3);
    }
}

inline void subBytes( uint8_t *in, uint8_t *out, bool inverse, uint8_t size) {
    QarmaMatrix_t temp;
    uint8_t tempi;
    uint8_t index;
    for(int i = 0; i < sizeof(sbox); i++) {
        index = get_cell((QarmaMatrix_t*)in, i);
        tempi = inverse ? sbox[index] : sbox_inv[index];
        set_cell(&temp, i, tempi);
    }
    copy_matrix((uint8_t *)&temp, out);
}

static inline void lfsr_tweak_cell_index(QarmaMatrix_t *inMatrix, uint8_t index) {
    uint8_t cell = 0;

    cell = get_cell(inMatrix, index);
    lfsr_tweak_cell(&cell);
    set_cell(inMatrix, index, cell);
}

inline void tweakLFSR(uint8_t *tweakIn, uint8_t *out, uint8_t size) {
    copy_matrix(tweakIn, out);
    QarmaMatrix_t *tweak = (QarmaMatrix_t*)out;

    for(int i = 0; i < sizeof(tweakLFSRPermutation); i++) {
        lfsr_tweak_cell_index(tweak, tweakLFSRPermutation[i]);
    }
}


inline void permuteTweak2(uint8_t *tweakIn, uint8_t *out, uint8_t size) {
    copy_matrix(tweakIn, out);
    QarmaMatrix_t *tweak = (QarmaMatrix_t*)out;
    uint8_t cell = 0;

    for(int i = 0; i < sizeof(tweakPermutation); i++) {
        cell = get_cell((QarmaMatrix_t*)tweakIn, tweakPermutation[i]);
        set_cell(tweak, i, cell);
    }
}

inline void calcTweak(uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t range) {
    QarmaMatrix_t temp;
    copy_matrix(tweakIn, out);
    for(int i = 0; i < range; i++) {
        permuteTweak2(out, (uint8_t *)&temp, size);
        tweakLFSR((uint8_t *)&temp, out, size);
    }
}
inline void calcRoundTweakKey(uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t *k0, uint8_t round, bool reverse) {
    calcTweak(tweakIn, out, size, round);
    xorBlock(out, k0, out, size);
    xorBlock(out, roundConstant[round], out, size);
    if(reverse) {
        xorBlock(out, alpha, out, size);
    }

}

inline void doRound(uint8_t *state, uint8_t *tweakIn, uint8_t *out, uint8_t size, uint8_t round, bool reverse) {
    QarmaMatrix_t temp;
    QarmaMatrix_t temp2;

    if(!reverse) {
        xorBlock(state, tweakIn, (uint8_t *)&temp, size);
        if(round > 0) {
            permuteState((uint8_t *)&temp, (uint8_t *)&temp2, false, size);
            mixColums(&temp2);
        } else {
            copy_matrix((uint8_t *)&temp, (uint8_t *)&temp2);
        }
        subBytes((uint8_t *)&temp2, out, false, size);

    } else {
        subBytes(state, (uint8_t *)&temp, true, size);
        if(round > 0) {
            mixColums(&temp);
            permuteState((uint8_t *)&temp, (uint8_t *)&temp2, true, size);
        } else {
            copy_matrix((uint8_t *)&temp, (uint8_t *)&temp2);
        }
        xorBlock((uint8_t *)&temp2, tweakIn, out, size);

    }
}

inline void middleRound(uint8_t *state, uint8_t *k1, uint8_t *out, uint8_t size) {
    QarmaMatrix_t temp;
    QarmaMatrix_t temp2;
    permuteState(state, (uint8_t *)&temp, false, size);
    mixColums(&temp);
    xorBlock((uint8_t *)&temp, k1, (uint8_t *)&temp2, size);
    permuteState((uint8_t *)&temp2, out, true, size);
}

static inline uint64_t matrixToUint64(QarmaMatrix_t *inMatrix) {
    uint64_t result = 0;
    uint64_t temp = 0;
    for(int i = 0; i < 16; i++) {
        temp = get_cell(inMatrix, i);
        result = result | (temp << (60 -(i * 4)));
    }
    return result;
}

static inline void uint64ToMatrix(uint64_t in, QarmaMatrix_t *out) {
    for(int i = 0; i < 16; i++) {
        set_cell(out, i, (in >> (60 -(i * 4))) & 0xF);
    }
}

inline uint64_t qarma64( uint8_t plaintext[8], uint8_t tweak[8], uint8_t key[16], bool encrypt, uint8_t rounds) {
    uint64_t result = 0;
    uint64_t w0d = 0;
    uint8_t w0[8];
    uint8_t w1[8];
    uint8_t k0[8];
    uint8_t k1[8];
    uint8_t temp[8];
    uint8_t state[8];
    uint8_t cipher[8];
    uint8_t tweakkey[8];

    memcpy(w0, key, 8);
    memcpy(k0, key+8, 8);
    // printf("w0 = 0x%lx\n", *(uint64_t *)&w0);
    // printf("k0 = 0x%lx\n", *(uint64_t *)&k0);
    // printf("plaintext = 0x%lx\n", *(uint64_t *)plaintext);
    // printf("tweak = 0x%lx\n", *(uint64_t *)tweak);

    w0d = matrixToUint64( (QarmaMatrix_t *)w0);
    result = ror64(w0d, 1) ^ (w0d >> 63) ;
    uint64ToMatrix(result, (QarmaMatrix_t *)w1);

    if(encrypt) {
        memcpy(k1, k0, 8);
    } else {
        memcpy(temp, w0, 8);
        memcpy(w0, w1, 8);
        memcpy(w1, temp, 8);

        memcpy(temp, k0, 8);
        mixColums((QarmaMatrix_t *)temp);
        memcpy(k1, temp, 8);
        xorBlock(k0, alpha, k0, 8);
    }

    xorBlock(plaintext, w0, state, 8);
    for(int i = 0; i < rounds; i++) {
        calcRoundTweakKey(tweak, tweakkey, 8, k0, i, false);
        doRound(state, tweakkey, state, 8, i, false);
    }
    //calcTewak
    calcTweak(tweak, tweakkey, 8, rounds);
    //round
    xorBlock(w1, tweakkey, temp, 8);
    doRound(state, temp, state, 8, rounds, false);
    //middleround
    middleRound(state, k1, state, 8);

    //round
    xorBlock(w0, tweakkey, temp, 8);
    doRound(state, temp, state, 8, rounds, true);

    for(int i = rounds - 1; i >= 0 ; i--) {
        calcRoundTweakKey(tweak, tweakkey, 8, k0, i, true);
        //round
        doRound(state, tweakkey, state, 8, i, true);
    }
    xorBlock(state, w1, cipher, 8);

    result = matrixToUint64( (QarmaMatrix_t *)cipher);
    return result;
}
