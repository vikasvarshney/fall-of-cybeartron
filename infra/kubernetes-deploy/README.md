# Deployment guide

## Deploying with Rancher

Acquire a kubeconfig from AWS EKS:

```bash
# Get and configure awscli if you haven't already:
pip3 install --user --upgrade awscli
aws configure
# Enter your IAM key details, "text" as your output, and the region you deployed the EKS cluster to

# Now we'll get the kubeconfig
aws eks list-clusters
# Take the name of the cluster and use it in the following command
aws eks update-kubeconfig --kubeconfig ./aws.kubeconfig.yaml --name ${NAME}
export KUBECONFIG="`readlink -f ./aws.kubeconfig.yaml`"

# Done!
kubectl get nodes
```

## Testing a deployment
See [k3s](infra/kubernetes-deploy/k3s)

## Deploying the challenges

You will need:
- A Gitlab deploy token with the `read_registry` permission
- A kubeconfig for your kubernetes cluster

```bash
./create_secret.sh
kubectl create -f deployment.yaml
```

## Updating domain names

You will need:
- A configured AWS cli
- An AWS token
- Challenges deployed to your kubernetes

```bash
kubectl get services -o json > services.json
aws route53 list-hosted-zones
# You'll want the bit after /hostedzone/, that's the zone ID
./update_dns.py -z ${ZONE_ID} -j services.json
```

### Deleting an existing deployment

#### Deleting the DNS entries
```bash
aws route53 list-hosted-zones
# You'll want the bit after /hostedzone/, that's the zone ID
./update_dns.py --delete -z ${ZONE_ID}
```

#### Removing the cluster
You'll need to first delete the cluster in rancher, it'll clean up most things.
Rancher creates:
- A VPC (the Name is part of the URL in rancher)
- An EKS cluster
- An EC2 Autoscale group
- EC2 instances
- A LoadBalancer for inbound kubernetes traffic
- Security groups and ingress rules

The teardown procedure is:
- Delete in rancher
- Delete the EC2 LoadBalancer
- Delete the VPC
    - The VPC might complain that resources are in use. It takes ~3 minutes for everything to get nuked.
    - You can track down the resources and delete them manually if you need to.
