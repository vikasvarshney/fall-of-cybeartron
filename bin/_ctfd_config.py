import os.path
import passgen
import re
import requests
import time

from ctfd_api import CTFdAuthenticatedAPISession

import upload_pages

def extract_csrf_nonce(response_data):
    return re.search(rb'csrf_nonce = "([0-9a-f]{64})"', response_data).group(1)

def wait_for_ctfd(uri):
    print("Waiting for CTFd - this may take a while if the DB is new")
    for i in range(1, 31, 3):
        try:
            requests.get(uri)
        except requests.exceptions.ConnectionError as exc:
            print(exc)
            time.sleep(i)
        else:
            print("CTFd is ready!")
            break

def try_setup_ctfd(root_uri, state_dir):
    client = requests.session()     # To maintain the session for CSRF
    root_get_r = client.get(root_uri)
    if not root_get_r.url.endswith("/setup"):
        print("Existing CTFd doesn't need setup")
    else:
        print("New CTFd - setting it up!")
        csrf_nonce = extract_csrf_nonce(root_get_r.content)
        # XXX: Ghetto password generation for the admin user is ghetto
        with open(
            os.path.join(state_dir, ".ctfd_admin_pass"), "a+"
        ) as passfile:
            passfile.seek(0)
            admin_pass = passfile.read()
            if not admin_pass:
                admin_pass = passgen.passgen(length=64)
                passfile.write(admin_pass)
                print(
                    "Generated a password for the cybears user: %s"
                    % admin_pass
                )
                print("You'll lose it if you run git clean -x!")
        setup_data = {
            "ctf_name": "BSides Canberra CTF 2019",
            "name": "cybears",
            "email": "cybears@gmail.com",
            "user_mode": "teams",   # Make sure this is correct or CTFd will
                                    # just 500 error all over the place!
            "password": admin_pass,
            "nonce": csrf_nonce,
        }
        setup_post_r = client.post(root_get_r.url, data=setup_data)
        if setup_post_r.url != root_uri:
            raise Exception(
                "Redirect to home page after setup failed! "
                "Ended up at {!r} instead".format(setup_post_r.url)
            )

def run_extra_config(api_session):
    # Patch the config to set the theme!
    config_patch_r = api_session.patch(
        "configs",
        json={
            "ctf_theme": "cybears",
        }
    )
    assert config_patch_r.status_code == 200, config_patch_r.status_code

    # Upload the welcome, rules, and story pages (defined in upload_pages.py)
    pages_list = [
        {"mode": "new", "title": "Rules", "route": "rules", "content": "pages_rules.md"},
        {"mode": "new", "title": "Story", "route": "story", "content": "pages_story.md"},
        {"mode": "edit", "id": "1", "content": "pages_welcome.html"},
    ]
    for page in pages_list:
        upload_pages.upload_page(api_session, page)


def do_ctfd_config(ctfd_uri, state_dir):
    wait_for_ctfd(ctfd_uri)
    try_setup_ctfd(ctfd_uri, state_dir)
    # We should have a password we can use for API sessions now
    with open(os.path.join(state_dir, ".ctfd_admin_pass"), "r") as passfile:
        admin_pass = passfile.read()
    api_session = CTFdAuthenticatedAPISession(ctfd_uri, admin_pass)
    # And we can run any extra configuration and setup we need
    run_extra_config(api_session)
