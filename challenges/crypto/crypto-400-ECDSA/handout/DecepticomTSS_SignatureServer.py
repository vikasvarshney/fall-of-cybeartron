#!/usr/bin/python3

#requirements
#pip3 install pycrypto
#pip3 install ecdsa

from ecdsa import *
import Crypto.Util.Counter as C
import os
from random import SystemRandom
import json

from crypto400ECDSA_secret import pubkey, privkey, secret, flag

randrange = SystemRandom().randrange

#Setup elliptic curve
g = ecdsa.generator_192
n = g.order()

#Initialise random counter value
ctr = C.new(128, initial_value = randrange(2,n-1))

def print_menu():
	print("Welcome to signature server. Enter your choice:\n"
		  "(0) Print ECDSA public key\n"
		  "(1) Generate signature\n"
		  "(2) submit signature for flag\n"
	      "(q) quit\n")

def print_ecdsa_public_key():
	'''
	use public key via:
	from ecdsa import curves, ellipticcurve
	pubkey_point = ecdsa.ellipticcurve.Point(curves.NIST192p.curve, pubkey.point.x(), pubkey.point.y())
	pubkey = ecdsa.Public_key(g, pubkey_point)

	'''
	h = {"curve": "curves.NIST192p.curve", "x": pubkey.point.x(), "y" : pubkey.point.y() }
	print(json.dumps(h) + "\n")

def generate_signature(message, privkey):
	'''
	verify this signature via:
	v = ecdsa.ecdsa.Signature(r["r"], r["s"])
	pubkey.verifies(r["message"], v)
	'''

	#repeated k is a catastrophic security failure. Ensure this can't happen
	secret_k = int.from_bytes(ctr(), byteorder='big')
	#message is an integer
	signature = privkey.sign(message, secret_k)
	return signature

if __name__ == "__main__":

	cmd = ""

	while cmd != "q":
		print_menu()
		cmd = input("Enter a value: ").strip()
		#print("You entered :", cmd)

		if cmd == "0":
			#print("cmd = 0")
			print_ecdsa_public_key()
			continue

		if cmd == "1":
			#print("cmd = 1")
			message = randrange(2,n-1)
			signature = generate_signature(message, privkey)
			j = {"message" : message, "r" : signature.r, "s" : signature.s}
			print(json.dumps(j) + "\n")
			continue

		if cmd == "2":
			#print("cmd = 2")
			#generate random message
			challenge_message = randrange(2, n-1)
			#ask user to sign message
			user_input = input("Please sign this message " +json.dumps({"message": challenge_message}) +
			" and return a json string with a valid r and s signature\n" +
			"\nEnter response:"
			)
			#validate input
			try:
				j = json.loads(user_input)
			except (TypeError, ValueError):
				print('JSON Error 0: try a string like {"r": r_value, "s": s_value}\n')
				exit()

			if(type(j) != dict):
				print('JSON Error 1: try a string like {"r": r_value, "s": s_value}\n')
				exit()

			if ("r" not in j.keys() or "s" not in j.keys()):
				print('JSON Error 2: try a string like {"r": r_value, "s": s_value}\n')
				exit()

			try:
				user_r = int(j["r"])
				user_s = int(j["s"])
			except ValueError:
				print("r or s was not an integer\n")
				exit()

			#validate signature
			v = ecdsa.Signature(user_r, user_s)
			result = pubkey.verifies(challenge_message, v)

			#if true, print flag, else quit
			if result:
				print("Congratulations! Here is your flag: " + flag + "\n")
				exit()
			else:
				print("Incorrect signature\n")
				exit()

			continue

		print("Incorrect option\n")
