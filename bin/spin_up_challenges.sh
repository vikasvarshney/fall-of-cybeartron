#!/usr/bin/env bash
KUBECTL=${KUBECTL:-kubectl}

MY_PATH=`readlink -f "$0"`
ROOT_DIR="`dirname ${MY_PATH}`/../"
ROOT_DIR="`readlink -f ${ROOT_DIR}`"
echo "[+] Spinning up!"
kubectl create -f "${ROOT_DIR}/.deploy"
