#!/usr/bin/env python3
import argparse
import importlib
import os
import os.path
import pathlib
import sys

import _ctfd_config

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def start_ctfd(platform, ctfd_path):
    # Add a CTFd secret key if one doesn't already exist
    with open(
        os.path.join(ctfd_path, ".ctfd_secret_key"), "ab+"
    ) as secretfile:
        if not secretfile.read():
            secretfile.write(os.urandom(64))
    # Spin up the CTFd infra using the platform specific module
    platform_module = importlib.import_module("_ctfd_{}".format(platform), ".")
    ctfd_host = platform_module.start_ctfd(ctfd_path)
    # The CTFd may have an alternate name which we need to use instead of the
    # service name to get TLS validation
    print("The service name is {!r}".format(ctfd_host))
    ctfd_host = os.environ.get("REAL_CTFD_HOST", ctfd_host)
    # Now we can run the setup actions to make sure it's fully configured
    _ctfd_config.do_ctfd_config(ctfd_host, SCRIPT_DIR)
    # And spit out a line which a shell can eval
    print("export CTFD_HOST=%r" % ctfd_host)
    print("export CTFD_STATE_DIR=%r" % SCRIPT_DIR)

def stop_ctfd(platform, ctfd_path):
    platform_module = importlib.import_module("_ctfd_{}".format(platform))
    platform_module.stop_ctfd(ctfd_path)

def purge_ctfd(platform, ctfd_path):
    platform_module = importlib.import_module("_ctfd_{}".format(platform))
    platform_module.purge_ctfd(ctfd_path)

def parse_args():
    # Top level parser
    parser = argparse.ArgumentParser()
    parser.set_defaults(action=None)
    parser.set_defaults(platform=None)
    # We add top level subparsers to define which platform we're using
    sps = parser.add_subparsers()
    compose_sp = sps.add_parser("compose", help="Use docker-compose")
    compose_sp.set_defaults(platform="compose")
    k3s_sp = sps.add_parser("k3s", help="Use k3s kubectl")
    k3s_sp.set_defaults(platform="k3s")
    k8s_sp = sps.add_parser("k8s", help="Use k8s")
    k8s_sp.set_defaults(platform="k8s")
    k8s_sp.add_argument(
        "--k8s-config", action="store", metavar="KUBECONFIG",
        help="Use a specific config (defaults to environment KUBECONFIG)",
        type=pathlib.Path, default=os.environ.get("KUBECONFIG"),
    )
    # And then add action subparsers to each of those so that we have
    # reasonable natural feeling `./script <platform> <action>` command lines
    for platform in (compose_sp, k3s_sp, k8s_sp):
        # Subparsers for the actual actions we support
        sps = platform.add_subparsers()
        start_sp = sps.add_parser("start", help="Start CTFd",)
        start_sp.set_defaults(action=start_ctfd)
        stop_sp = sps.add_parser("stop", help="Stop CTFd")
        stop_sp.set_defaults(action=stop_ctfd)
        purge_sp = sps.add_parser("purge", help="Stop CTFd and purge state")
        purge_sp.set_defaults(action=purge_ctfd)
    # Now we parse the arguments and try to print subparser aware help if we're
    # missing one of our expected arguments using a janky append of `-h`
    args = parser.parse_args()
    if not args.action or not args.platform:
        # This will exit the script after printing the usage
        parser.parse_args(sys.argv[1:] + ["-h", ])
    return args

if __name__ == "__main__":
    args = parse_args()
    ctfd_path = os.path.join(SCRIPT_DIR, "..", "infra", "ctfd")
    try:
        args.action(args.platform, ctfd_path)
    except ModuleNotFoundError as exc:
        raise ModuleNotFoundError(
            "{} platform is not supported".format(args.platform)
        )
