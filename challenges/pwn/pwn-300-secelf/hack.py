#!/usr/bin/env python

from pwn import *

e = ELF('./dist/test.signed')
main_addr = e.symbols['main']

s = shellcraft.linux.echo("EVIL")

print(e.disasm(main_addr, 10))

log.success("Modifying...")
e.write(main_addr, asm(s))

print(e.disasm(main_addr, 10))

e.save('./dist/test.evil')
