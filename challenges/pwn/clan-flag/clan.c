
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

void getflag(int num)
{
    FILE *file = 0;
    char *flag = 0;
    size_t length = 0;

    file = fopen("./flag.txt", "r");
    if(file == 0)
        exit(1);

    if(getline(&flag, &length, file) == -1)
        exit(1);

    printf("Congratulations soldier number %d, you have been selected to carry the flag\nThe flag is %s\n", num, flag);
}

// By making this a double pointer, the optimizer doesn't remove the pointer variable from the stack in main()
void getnumber(int **num)
{
    srandom(time(0));
    **num = random() % 100;
}

void main(void)
{
    int num;
    int *pnum = &num;
    char fmt[] = "Clan          welcomes %s, you are soldier number %d\nSoldier 100 will be given the flag\n";
    char *name, *clan;

    getnumber(&pnum);
    setbuf(stdout, NULL); // unbuffer stdout
    printf("What is your name?\n"); 
    if(scanf("%ms", &name) <= 0) return;

    printf("What is your clan?\n");
    if(scanf("%8ms", &clan) <= 0) return;
    // This generates a compiler warning because it looks bad.......
    // but that is on purpose, I am fairly sure it isn't exploitable as the length is controlled by the scanf the line above
    strncpy(&fmt[5], clan, strlen(clan));

    printf(fmt, name, num);

    if(num == 100)
        getflag(num);
}
