-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  energon INTEGER NOT NULL,
  alliance TEXT NOT NULL
);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id)
);

-- alpha_trion: power user
-- from werkzeug.security import check_password_hash, generate_password_hash
--In [5]: generate_password_hash("MyAltModeIsHovercraft")
--Out[5]: 'pbkdf2:sha256:50000$3ErplE8v$26841ec55e356bad6ba9b627511213e2e9140aa98c3bd255828ecf5d77f1b762'


INSERT INTO user (username, password, energon, alliance)
VALUES
('alpha_trion', 'pbkdf2:sha256:50000$NDdGxnQT$12cec0da3d8023daaa8b24eaf78eee600f52088185c1f7dcd170087644f02927', '10', 'CYBEARS'),
-- user: lazerpaw password: 62dc123d2ae1292d3277b894
('lazerpaw', 'pbkdf2:sha256:50000$rw7g3RNm$a8bf1efaabb54dab43ba0722a5f740b850c97c64099cbe4963489737fd47f6ad', '9', 'DECEPTICOMTSS'),
-- user: bumblebear password: efd94c6f0dd59f5a1a89424a
('bumblebear', 'pbkdf2:sha256:50000$4TFS9rHs$a5b74db2dde466a22117e238dfcab7b7ae7fe7ea0b60cbbf34d3e083eabd826f', '8', 'CYBEARS'),
-- user: bamega_prime password: 68cfcd907883a8871b0f1e0b
('bamega_prime', 'pbkdf2:sha256:50000$cUuZcVvT$16ccb69df9e6bf27c1f523ce3e8b89c073ea255acf7d56d871dbbc34a608e71c', '4', 'CYBEARS'),
-- user: barbearian password: fdd3225062ef1c3f6cda5fc3
('barbearian', 'pbkdf2:sha256:50000$vjJonKb3$6686a28abdc027e95b87c70a503abec25823622530f7bb56d7b5eab28715357f', '8', 'DECEPTICOMTSS'),
-- user: bearricade password: 7f1ef78e2874dcb1c6173eb5
('bearricade', 'pbkdf2:sha256:50000$HkQTtJOh$45fb425bdb253b43c447d8f1ed0ff1ba6547eb1e5314d3ca450b5a5ddd5b03b6', '7', 'DECEPTICOMTSS'),
-- user: pandamonium password: 66f3f638aeefaaf33ba417cc
('pandamonium', 'pbkdf2:sha256:50000$QgusKLEH$0a3662e0eb046e12aeb9efc5bf5396614a7bc4726f45ebed15f515909fc9221c', '5', 'DECEPTICOMTSS'),
-- user: richotron password: 2f884e36a4fa0148f3344cfc
('richotron', 'pbkdf2:sha256:50000$D6AOQCJk$17c0e58e867816af3ccb1a09baa71c8e7d5856e6171ac754beab5d02187d224b', '2', 'DECEPTICOMTSS'),
-- user: ravage password: 6bc5e8301f9755c735b714cd
('ravage', 'pbkdf2:sha256:50000$cHyg6eQ1$854a220f853687d38fcdc7805ea70419e0ed223fd3978233ac94194fe4924549', '3', 'DECEPTICOMTSS'),
-- user: polar password: 57054e4c9fbffd840a0c3afc
('polar', 'pbkdf2:sha256:50000$F83Xng9M$df0ba1b92b751388a090a7c102f8de96dbbba1e457d0ad9ac14d8dd9c2a2cd88', '0', 'CYBEARS'),
-- user: snarl password: 9abbbdf4a0feeeca8ff49c61
('snarl', 'pbkdf2:sha256:50000$g76eJOQH$e8e32e654078449cacad98701d186e1f26650c554ddfe1a8ea578e87a62f6b76', '6', 'CYBEARS'),
-- user: long_tooth password: 690c74aa8bbfd4d9c1ce4a73
('long_tooth', 'pbkdf2:sha256:50000$jkf9l74Z$09c88c4267d8a283b04eb057d5b15dea15cc213bcd23c7e3a3454a9269d3f655', '5', 'CYBEARS'),
-- user: grizzlytron password: 75af64a8d64784a18b0ff9a3
('grizzlytron', 'pbkdf2:sha256:50000$GY9TdcFC$17a4401e5fafee869cfcca5d959e02b3e854e9055718fe3023cd9255771da019', '7', 'DECEPTICOMTSS'),
-- user: ursine_magnus password: 3e3344a37a61c63037bb6f61
('ursine_magnus', 'pbkdf2:sha256:50000$h5FVBYqQ$7dc270ae7d7a0f58a66c7bce77ebb1a6da7854523a7234012c48d3ab6659f039', '4', 'CYBEARS'),
-- user: prowl password: d1561db192c2ef604c8d442b
('prowl', 'pbkdf2:sha256:50000$wFmAtYXk$c7a838ef411a5e1c3e2b4a0a18ec7878f0f478106d9fdea7027998688a12d80d', '9', 'CYBEARS');



INSERT INTO post (author_id, title, body) 
VALUES
(9, 'Destroy all Cybears!', 'Destroy all Cybears!'),
(1, 'Till all are one', 'To show the true spirit of togetherness, I will give new users two new energon! Till all are one!'),
(5, 'Knowledge', 'Your knowledge is only overshadowed by your stupidity!'),
(12, 'Energy crisis', 'If a new source of energy is not found, no one is going to win this war'),
(2, 'Courage', 'It requires more courage to suffer than to die'),
(4, 'Sacrifice', 'No sacrifice, no victory'),
(2, 'Power and desire', 'Power flows to the one who knows how. Desire alone is not enough'),
(3, 'Ive got better things to do tonight than die', 'Ive got better things to do tonight than die'),
(5, 'SHOW NO MERCY!', 'SHOW NO MERCY!'),
(7, 'Aspirations', 'Everybody wants to rule the universe'),
(9, 'The powers of darkness', 'The powers of darkness are greater than anything your pathetic scientific toys can muster'),
(7, 'Enemies', 'Sometimes its better to be known for ones enemies'),
(5, 'Cybears', 'Yuck. Noble Cybears make me wanna puke.'),
(5, 'Prepare for extermination', 'Prepare for extermination'),
(5, 'I will rule the universe', 'I will rule the universe, even if I am the only one left in the universe'),
(11, 'Honor', 'Honor to the end'),
(13, 'Questions', 'why are there logs under the pcap folder?'),
(11, 'Freedom', 'Freedom is the right of all sentient beings'),
(15, 'Wisdom', 'Even the wisest of men and machines can be in error'),
(1, 'Universal greeting', 'Bah weep gragnah weep nini bong'),
(4, 'Heroism', 'Theres a thin line between being a hero and being a memory');


