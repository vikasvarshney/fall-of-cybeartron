import logging
import os
import os.path
import subprocess
import yaml

logging.basicConfig(level=logging.INFO)

class NotFound(BaseException):
    pass

class Handout(object):
    def __init__(self, handout_path, root_path):
        self._path = handout_path
        self._file = open(os.path.join(root_path, handout_path), "rb")

    @property
    def path(self):
        return self._path

    @property
    def file(self):
        return self._file

class Deployment(object):
    def __init__(self, deployment_attrs, root_path):
        """
        Represents a deployment config for k8s

        Deployment attrs is a dict containing values from the deployment section
        """
        self._root_path = root_path
        attrs = deployment_attrs if deployment_attrs is not None else dict()
        self._attrs = attrs
        
    @property
    def deploy_name(self):
        return self._attrs.get("deploy_name")
    @deploy_name.setter
    def deploy_name(self, new_deploy_name):
        self._attrs["deploy_name"] = new_deploy_name

    @property
    def container_image(self):
        return self._attrs.get("container_image")
    @container_image.setter
    def container_image(self, new_container_image):
        self._attrs["container_image"] = new_container_image

    @property
    def healthcheck_image(self):
        return self._attrs.get("healthcheck_image")
    @healthcheck_image.setter
    def healthcheck_image(self, new_healthcheck_image):
        self._attrs["healthcheck_image"] = new_healthcheck_image

    @property
    def solve_script(self):
        return self._attrs.get("solve_script")
    @solve_script.setter
    def solve_script(self, new_solve_script):
        self._attrs["solve_script"] = new_solve_script

    @property
    def target_port(self):
        return int(self._attrs.get("target_port"))
    @target_port.setter
    def target_port(self, new_target_port):
        self._attrs["target_port"] = new_target_port

    @property
    def target_dns_subdomain(self):
        return self._attrs.get("target_dns_subdomain")
    @target_dns_subdomain.setter
    def target_dns_subdomain(self, new_target_dns_subdomain):
        self._attrs["target_dns_subdomain"] = new_target_dns_subdomain

    @property
    def replicas(self):
        return int(self._attrs.get("replicas", 2))
    @replicas.setter
    def replicas(self, new_replicas):
        self._attrs["replicas"] = replicas

    @property
    def healthcheck_interval(self):
        return int(self._attrs.get("healthcheck_interval", 30))
    @healthcheck_interval.setter
    def healthcheck_interval(self, new_healthcheck_interval):
        self._attrs["healthcheck_interval"] = new_healthcheck_interval

    DEFAULT_TEMPLATE = os.path.normpath(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "..", "infra", "kubernetes-deploy", "template_deployment.yml"
    ))
    @property
    def template(self):
        template_path = self._attrs.get("template", self.DEFAULT_TEMPLATE)
        if not os.path.isabs(template_path):
            template_path = os.path.join(self._root_path, template_path)
        if not os.path.isfile(template_path):
            raise FileNotFoundError(template_path)
        return template_path
    @template.setter
    def template(self, new_template_path):
        norm_path = os.path.normpath(new_template_path)
        if not os.path.isfile(norm_path):
            raise FileNotFoundError(new_template_path)
        self._attrs["template"] = norm_path

class Challenge(object):
    """
    An object which describes a CTF challenge and its associated data.
    """
    def __init__(self, root_path, attrs=None, need_handouts=True):
        self._root_path = root_path
        attrs = attrs if attrs is not None else dict()
        if need_handouts:
            # Concretise the handouts we were initialised with
            handouts = attrs.get("handouts", list())
            try:
                attrs["handouts"] = list(
                    Handout(handout, root_path) for handout in handouts or list()
                )
            except FileNotFoundError:
                # We can try to build the challenge
                self.try_build()
                # And now retry the handout loading
                attrs["handouts"] = list(
                    Handout(handout, root_path) for handout in handouts or list()
                )
        self._attrs = attrs

    @classmethod
    def from_manifest(cls, manifest_path, need_handouts=True):
        """
        Load a challenge MANIFEST.yml and yield `Challenge` objects from it.
        """
        with open(manifest_path) as mf:
            manifest_documents = yaml.load_all(mf)
            for manifest_document in manifest_documents:
                # MANIFESTs may only contain one challenge block under this key
                challenge = manifest_document.get("challenge")
                yield cls(
                    os.path.dirname(manifest_path), challenge,
                    need_handouts=need_handouts
                )

    ###########################################################################
    # Simple properties
    ###########################################################################
    @property
    def name(self):
        return self._attrs.get("name", "Unnamed challenge")
    @name.setter
    def name(self, new_name):
        self._attrs["name"] = new_name

    @property
    def deploy_name(self):
        return self.deployment.deploy_name if self.deployment and self.deployment.deploy_name else self.name

    @property
    def category(self):
        return self._attrs.get("category", "No category")
    @category.setter
    def category(self, new_category):
        self._attrs["category"] = new_category

    @property
    def value(self):
        return int(self._attrs.get("value", 0))
    @value.setter
    def value(self, new_value):
        self._attrs["value"] = new_value

    @property
    def deployment(self):
        d = self._attrs.get("deployment")
        return Deployment(d, root_path=self._root_path) if d else None
    @value.setter
    def value(self, new_value):
        if not isinstance(new_value, Deployment) and not isinstance(new_value, dict):
            raise AttributeError("deployment must be a Deployment object or a dictionary")
        self._attrs["deployment"] = new_value

    @property
    def description(self):
        return self._attrs.get("description", "No description")
    @description.setter
    def description(self, new_description):
        self._attrs["description"] = new_description

    @property
    def use(self):
        # We use challenges by default unless they opt out with a YAML value
        # which ends up being falsey in Python. Normally `use: false` would be
        # the expected trope for opt-out challenges.
        return int(self._attrs.get("use", True))
    @use.setter
    def use(self, new_use):
        self._attrs["use"] = new_use

    ###########################################################################
    # Iterables
    ###########################################################################
    @property
    def tags(self):
        return self._attrs.get("tags", list())
    @tags.setter
    def tags(self, new_tags):
        self._attrs["tags"] = list(new_tags)
    def add_tag(self, new_tag):
        self.tags.append(new_tag)

    @property
    def handouts(self):
        return self._attrs.get("handouts", list())
    @handouts.setter
    def handouts(self, new_handouts):
        self._attrs["handouts"] = new_handouts
    def add_handout(self, new_handout):
        # Handouts need to be conretised
        self.handouts.append(Handout(new_handout, self._root_path))

    @property
    def flags(self):
        return self._attrs.get("flags", list())
    @flags.setter
    def flags(self, new_flags):
        self._attrs["flags"] = new_flags
    def add_handout(self, new_flag):
        # Handouts need to be conretised
        self.flags.append(new_flag)

    ###########################################################################
    # General helpers
    ###########################################################################
    def find_first_file(self, filename):
        for path, __, files in os.walk(self._root_path):
            if filename in files:
                return os.path.join(self._root_path, path, filename)

    ###########################################################################
    # Build processes
    ###########################################################################
    def try_build(self):
        try:
            if self.try_build_sh():
                return
        except NotFound:
            pass
        try:
            self.try_cmake()
        except NotFound:
            pass
        try:
            self.try_make()
        except NotFound:
            logging.info("Unable to build anything for %r", self._root_path)

    def try_cmake(self):
        top_level_cmakelist = self.find_first_file("CMakeLists.txt")
        if top_level_cmakelist is None:
            raise NotFound("No CMakeLists.txt present")
        # XXX: Maybe we should create a build directory? The MANIFEST is
        # hardcoded to know where handouts live though so for the moment we'll
        # just do everything in the challenge root and hope nobody requires a
        # separate build tree...
        cmake_srcdir = os.path.dirname(top_level_cmakelist)
        return self._try_somewhere(("cmake", "."), cmake_srcdir)

    def try_make(self):
        top_level_makefile = self.find_first_file("Makefile")
        if top_level_makefile is None:
            raise NotFound("No Makefile present")
        makefile_dir = os.path.dirname(top_level_makefile)
        return self._try_somewhere(("make", ), makefile_dir)

    def try_build_sh(self):
        build_sh = self.find_first_file("build.sh")
        if build_sh is None:
            raise NotFound("No build.sh")
        build_dir = os.path.dirname(build_sh)
        return self._try_somewhere((build_sh, ), build_dir)

    @staticmethod
    def _try_somewhere(cmd, cwd):
        logging.info(cmd)
        logging.info(cwd)
        try:
            subprocess.check_output(
                cmd, cwd=cwd, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as exc:
            logging.warn(
                "Unable to run %r at %r: %s", cmd, cwd, exc
            )
            logging.info(exc.stdout)
        else:
            return True
