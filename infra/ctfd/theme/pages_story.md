<div class="jumbotron">
	<h1 class="cybeartron preslash postslash">The Story so far</h1>
</div>

##Past

The noble Cybears have been at war with their arch nemeses, DecepticomTSS, for
eons. The war has raged for so long, most warriors don't even remember what
started the conflict. The Cybears leader, Bamega Prime, seeks peace and unity,
but in a recent battle, the Cybears lost their homeworld of Cybeartron.

##Present

You join the Cybears in their epic battle against the DecepticomTSS. You will
need to use your wits, knowledge and cyber security skills to understand your
enemies plans, defend your fellow Cybears and come out victorious!

However, is everything as it seems? Who is the real enemy in this conflict?

Sign up to the war [here](register).
