#!/bin/bash
# Run this script with $1 as GOLIVE to actually push to docker

# Login to docker.io .. really this is https://index.docker.io/v1/
docker login -u cybears -p Cy8earsCTFCy8earsCTF

# Build the image containing the flag
# Remove for the real image <---
docker build -t cybears/level0 -f level0/Dockerfile level0/

# Push to the repo if script was called with GOLIVE
if [ "$1" != "GOLIVE" ]; then echo "Not pushing - but test build complete."; exit 1; fi
docker push cybears/level0
