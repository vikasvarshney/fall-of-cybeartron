#!/usr/bin/env python3
import os
import os.path
import logging
import argparse
import glob
from string import Template


from challenge import Challenge
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
REPO_BASE = os.path.normpath(os.path.join(SCRIPT_DIR, ".."))
DEPLOYMENT_NAME = "deployment.yaml"
NEXT_PORT = 3000

def create_k8s_deploy(challenge, registry, dns, output_dir, force=False, reuse_ports=True):
    deploy_script_path = os.path.join(output_dir, challenge.deploy_name + '.' + DEPLOYMENT_NAME)
    if challenge.deployment:
        logging.info("Creating deploy script for {}..".format(challenge.name))
        if not challenge.use and not force:
            logging.info(
                "Skipping %r since it opts out of use and we weren't forced",
                challenge.name
            )
            return
        if os.path.isfile(deploy_script_path) and not force:
            logging.info("Skipping existing file {}".format(deploy_script_path))
            return
        with open(challenge.deployment.template) as template_file:
            template = Template(template_file.read())
            full_domain = challenge.deployment.target_dns_subdomain
            if dns:
                full_domain = full_domain + '.' + dns
            if reuse_ports:
                loadbalancer_port = challenge.deployment.target_port
            else:
                global NEXT_PORT
                NEXT_PORT += 1
                loadbalancer_port = NEXT_PORT
            patched_solve_script = Template(challenge.deployment.solve_script).substitute(
    
                target_port=challenge.deployment.target_port,
                target_dns=full_domain,
            )
            with open(deploy_script_path, "w") as deployment_file:
                deployment_file.write(template.substitute(
                    name=challenge.deploy_name,
                    category=challenge.category,
                    server_container=registry + challenge.deployment.container_image,
                    target_port=challenge.deployment.target_port,
                    loadbalancer_port=loadbalancer_port,
                    target_dns=full_domain,
                    healthcheck_container=registry + challenge.deployment.healthcheck_image,
                    solve_script=patched_solve_script,
                    replicas=challenge.deployment.replicas,
                    healthcheck_interval=challenge.deployment.healthcheck_interval,
                    registry=registry
                ))

def discover_manifests(challenge_base, registry, dns, force, output_dir, reuse_ports):
    for manifest in glob.glob(challenge_base + '/**/MANIFEST.yml', recursive=True):
        logging.info("Inspecting {}".format(manifest))
        for challenge in Challenge.from_manifest(
            manifest, need_handouts=False
        ):
            create_k8s_deploy(
                challenge, registry, dns,
                output_dir, force, reuse_ports
            )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')
    parser.add_argument('-f', '--force', action='store_true', default=False, help='Force creation of deployment scripts, will overwrite deployment.yaml files if they already exist')
    parser.add_argument('-r', '--registry', default='registry.gitlab.com/cybears/fall-of-cybeartron/', help='Base container registry location')
    parser.add_argument('-d', '--dns', default='ctf.cybears.io', help='Base domain for the the hosted ctf')
    parser.add_argument('-o', '--output-dir', default=os.path.join(REPO_BASE, '.deploy'), help='Directory to place the deployment yamls in. Defaults to .deploy in the repo root.')

    # Local testing stuff
    parser.add_argument('--no-port-reuse', action='store_true', help="Don't reuse ports for loadbalancers. Useful when deploying locally on minikube or k3s, etc")
    parser.add_argument('--no-domain', action='store_true', help="Don't set a domain for loadbalancers, just uses subdomain. Useful when deploying locally on minikube or k3s, etc")
    parser.add_argument('--start-port', type=int, default=3000, help='The port to start with when assigning ports to the load balancers. Only useful when  the --no-port-reuse flag is specified.')

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    if args.no_domain:
        args.dns = None

    if not os.path.isdir(args.output_dir):
        os.mkdir(args.output_dir)
    NEXT_PORT = args.start_port-1
    discover_manifests(os.path.join(REPO_BASE, "challenges"), registry=args.registry, dns=args.dns, force=args.force, output_dir=args.output_dir, reuse_ports=not args.no_port_reuse)

