#!/usr/bin/env python3
import argparse
import os
import os.path
import pathlib
import re
import string
import unicodedata
import yaml
import zipfile

from challenge import Challenge

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BUNDLE_DIR = os.path.normpath(os.path.join(SCRIPT_DIR, "..", ".bundle"))

def discover_manifests(paths):
    for path in paths:
        for name, _, files in os.walk(path.absolute()):
            if "MANIFEST.yml" in files:
                manifest_path = os.path.join(path, name, "MANIFEST.yml")
                yield os.path.realpath(manifest_path)

def load_manifest(manifest_path):
    print("Loading challenges from {}".format(manifest_path))
    yield from Challenge.from_manifest(manifest_path)

def slugify(value):
    value = unicodedata.normalize('NFKD', value)
    value = value.encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)

# https://stackoverflow.com/a/33300001
# It seems like the pipe string style doesn't always take in the dumped
# manifest but that's okay, it's just a little jank ;)
def str_presenter(dumper, data):
    if len(data.splitlines()) > 1:  # check for multiline string
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
    return dumper.represent_scalar('tag:yaml.org,2002:str', data)
yaml.add_representer(str, str_presenter)

def build_bundle(challenge):
    print("Bundling challenge release for %r" % challenge.name)
    # Substitute template macros in the challenge description
    if challenge.deployment is not None:
        description = string.Template(challenge_obj.description).substitute(
            target_dns = "run-it-yourself.cool",
            target_port = "31337",
        )
    else:
        description = challenge.description
    # Rebuild a releasable MANIFEST from the `Challenge` object
    manifest = yaml.dump({
        "challenge": {
            "name": challenge.name,
            "category": challenge.category,
            "tags": challenge.tags,
            "description": description,
            "handouts": list(h.path for h in challenge.handouts),
        }
    })
    # Now create the bundle!
    challenge_bundle_fname = os.extsep.join((slugify(challenge.name), "zip"))
    challenge_bundle_path = os.path.join(BUNDLE_DIR, challenge_bundle_fname)
    try:
        os.makedirs(BUNDLE_DIR)
    except FileExistsError:
        pass
    with zipfile.ZipFile(challenge_bundle_path, mode="w") as bundle:
        bundle.writestr(
            os.path.join(slugify(challenge.name), "MANIFEST.yml"), manifest
        )
        for handout in challenge.handouts:
            bundle.writestr(
                os.path.join(slugify(challenge.name), handout.path),
                handout.file.read()
            )

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "search_path", nargs="*", type=pathlib.Path,
        default=(pathlib.Path(__file__).parent.parent / "challenges", ),
    )
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    for manifest_path in discover_manifests(args.search_path):
        for challenge_obj in load_manifest(manifest_path):
            build_bundle(challenge_obj)
