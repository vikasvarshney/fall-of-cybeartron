// I hope you loaded all the things you needed... HTML should read:

// <!-- load the three.js and WebGL support -->
// <script src="js/3d/three.min.js"></script>
// <script src="js/3d/WebGL.js"></script>

// <!-- Post processing bloom filter begins here - credit to mrdoob -->
// <script src="js/3d/postprocessing/EffectComposer.js"></script>
// <script src="js/3d/postprocessing/RenderPass.js"></script>
// <script src="js/3d/postprocessing/ShaderPass.js"></script>
// <script src="js/3d/shaders/CopyShader.js"></script>
// <script src="js/3d/shaders/LuminosityHighPassShader.js"></script>
// <script src="js/3d/postprocessing/UnrealBloomPass.js"></script>

// <!-- load the STL loading module for the 3d meshes -->
// <script src="js/3d/loaders/STLLoader.js"></script>
// <script src="/themes/cybears/static/js/3d/loaders/GLTFLoader.js"></script>

// <!-- load the amatuer retro stylings and actually render -->
// <script src="/themes/cybears/static/js/3d/cybeartron.js"></script>

// Then go:

// Check for WebGL .. error only in console to be nice
if ( WEBGL.isWebGLAvailable() === false ) {
    document.body.appendChild( WEBGL.getWebGLErrorMessage() );
}

// let's just make most things global because I don't know javascript
var container;
var camera, scene, renderer;
var cybears = new THREE.Mesh();
var decepticomTSS = new THREE.Mesh();

// better hide some f l a g stuff in here I guess
// challenge name: No school like the old school
var keyMulti = 1;
var code = [38,38,40,40,37,39,37,39,66,65, 13], key = 0;
var codeon = true;

// framerate cap-ish. Change to 30 if people have performance issues.
var framerate = 60;
var slowdown = framerate;

// some controls if the camera is unhooked
var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var canJump = false;
var tragedy = false;

var prevTime = performance.now();
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();

var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

// back when the code started out this nice init function
// set everything up. not as clean now, ohwell, but still important
init();

// Leave in a testloader for stl models for testing during gltf failure town
var testloader = new THREE.STLLoader();

// Pull in the glTFs for cybears / decepticomTSS
var gltfloader = new THREE.GLTFLoader().setPath( 'themes/cybears/static/models/' );

gltfloader.load( 'decepticomTSS.gltf', function ( gltf ) {

    decepticomTSS = gltf.scene;

    decepticomTSS.position.set( 0, 15, -180 );
    decepticomTSS.rotation.set( 0, - Math.PI / 18, 0 );
    decepticomTSS.scale.set( 150, 150, 150 );

    decepticomTSS.castShadow = true;
    decepticomTSS.receiveShadow = true;

    scene.add( decepticomTSS );
}, undefined, function ( e ) {
    console.error( e );
} );

gltfloader.load( 'cybears.gltf', function ( gltf ) {

    cybears = gltf.scene;

    cybears.position.set( 0, 0, 180 );
    cybears.rotation.set( 0, Math.PI / 18, 0 );
    cybears.scale.set( 165, 165, 165 );

    cybears.castShadow = true;
    cybears.receiveShadow = true;

    scene.add( cybears );

}, undefined, function ( e ) {
    console.error( e );
} );

// Sweet bloom filter requirements
var composer, mixer, pointLight;
var clock = new THREE.Clock();
var renderScene = new THREE.RenderPass( scene, camera );

var params = {
    exposure: 0.8,
    bloomStrength: 1.3,
    bloomThreshold: 0,
    bloomRadius: 0.8
};

var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );

bloomPass.renderToScreen = true;
bloomPass.threshold = params.bloomThreshold;
bloomPass.strength = params.bloomStrength;
bloomPass.radius = params.bloomRadius;

// Renderer (can be run without the bloom pass - hence the weird composer double-up)
renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
renderer.setPixelRatio( window.devicePixelRatio );
renderer.setSize( window.innerWidth, window.innerHeight);
renderer.setClearColor( 0x010103 );

// I don't think I know which tone change I like anymore. Pretty retro though!
// Options: ReinhardToneMapping, Uncharted2ToneMapping, CineonToneMapping, ACESFilmicToneMapping
renderer.toneMapping = THREE.ReinhardToneMapping;
container.appendChild( renderer.domElement );

composer = new THREE.EffectComposer( renderer );
composer.setSize( window.innerWidth, window.innerHeight);
composer.addPass( renderScene );
composer.addPass( bloomPass );


// LIGHTING, could totally tweak this for better effect

// sweet global metallic stylings
scene.add( new THREE.AmbientLight( 0x404040 ) );

// retro light
scene.add( new THREE.HemisphereLight( 0x443333, 0x111122 ) );

// let's kick it up a notch
addShadowedLight( -10, -10, -10 , 0xffffff, 0.2 );
addShadowedLight( 10, 10, 10, 0xffffff, 0.2 );
addShadowedLight( 10, 10, -10, 0xddaa33, 0.5 );
// addShadowedLight( -10, -10, 10, 0xffaa00, 0.6 );

function addShadowedLight( x, y, z, color, intensity ) {

    var directionalLight = new THREE.DirectionalLight( color, intensity );
    directionalLight.position.set( x, y, z );
    scene.add( directionalLight );

    directionalLight.castShadow = true; // I really don't need this with the boring grid

}

// call this after setting up stls bloom lights etc
animate();

function init() {

    // container = document.createElement( 'div' );
    container = document.getElementById('cygrid');
    document.body.appendChild( container );

    // we'll just use one
    camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 2, 2000 );
    camera.position.set( 400, -70, 0 );

    // no point having a camera without a scene
    scene = new THREE.Scene();

    // Let's make the background fade into fog
    scene.fog = new THREE.Fog( 0x000000, 150, 1000 );

    // Plain grid - 0xb153e8, 0x1e83d3
    var retrogrid = new THREE.GridHelper( 2500, 65, 0xb153e8, 0x1e83d3 );
    retrogrid.position.y = - 75;
    scene.add( retrogrid );

    // Allow resize
    window.addEventListener( 'resize', onWindowResize, false );

    // Add mouse listeners
    document.addEventListener( 'mousemove', onDocumentMouseMove, false );

    // Keyboard too
    initKeyDown();

}

function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );

}

// Follow mouse yo
function onDocumentMouseMove( event ) {
    mouseX = ( event.clientX - windowHalfX );
    mouseY = ( event.clientY - windowHalfY );
}

// Key presses .. why track these?
function initKeyDown() {
    // props to bank of canada web team for the better method
    function onKeyDown( event ) {
        if (codeon) {
            if (code[key++] == event.keyCode) {
                if (key == 11) {
                    // If the code was completed start/stop it.
                    console.log("-~= KONAMI!!! =~-");

                    // Play the special tune - ALTTP maybe best, but this is OoT
                    m = new Audio("themes/cybears/static/img/secret.mp3");
                    m.volume = 0.6;
                    m.loop = false;
                    m.play();
                    keyMulti += -20;

                    // unhook the code lock
                    codeon = false;
                    key = 0;

                    // Bring in the f l a g
                    // You could decode this manually but you shouldn't : D
                    eval(function(p,a,c,k,e,r){e=String;if(!''.replace(/^/,String)){while(c--)r[c]=k[c]||c;k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('0();',2,1,'load_test'.split('|'),0,{}))
                }
                else if ( key == 10 ) {
                    m = new Audio("themes/cybears/static/img/smw_1-up.mp3");
                } else {
                    m = new Audio("themes/cybears/static/img/smw_coin.mp3");
                }
                m.volume = 0.8;
                m.loop = false;
                m.play();

            } else {
                // Reset if the wrong key was pressed in the secret sequence
                key = 0
            }
        } else {
            switch ( event.keyCode ) {
                case 38: // up
                case 87: // w
                    moveForward = true;
                    break;
                case 37: // left
                case 65: // a
                    moveLeft = true;
                    break;
                case 40: // down
                case 83: // s
                    moveBackward = true;
                    break;
                case 39: // right
                case 68: // d
                    moveRight = true;
                    break;
                case 32: // space
                    if ( canJump === true ) velocity.y += 350;
                    if (Math.random() > 0.5) {
                        m = new Audio("themes/cybears/static/img/contra-1.mp3");
                    } else {
                        m = new Audio("themes/cybears/static/img/contra-2.mp3");
                    }
                    m.volume = 0.7;
                    m.loop = false;
                    m.play();
                    canJump = false;
                    break;
            }
        }
    }
    function onKeyUp( event ) {
        if (codeon) {
            if (event.keyCode == 32) {
                keyMulti += 180;
            }
        } else {
            switch ( event.keyCode ) {
                case 38: // up
                case 87: // w
                    moveForward = false;
                    break;
                case 37: // left
                case 65: // a
                    moveLeft = false;
                    break;
                case 40: // down
                case 83: // s
                    moveBackward = false;
                    break;
                case 39: // right
                case 68: // d
                    moveRight = false;
                    break;
            }
        }
    }
    document.addEventListener( 'keydown', onKeyDown, false );
    document.addEventListener( 'keyup', onKeyUp, false );
}


// Animate with a cap on framerate
function animate() {

    setTimeout( function() {

        requestAnimationFrame( animate );

    }, 1000 / framerate );

    var time = performance.now();
    var delta = ( time - prevTime ) / 1000;

    if (!codeon) {

        if (slowdown != 0 ) {
            slowdown -= 1;
            camera.rotation.x -= camera.rotation.x * (framerate - slowdown) / framerate;
            camera.rotation.z -= camera.rotation.z * (framerate - slowdown) / framerate;
            camera.position.y -= (40 * (framerate - slowdown) / framerate) + (camera.position.y * (framerate - slowdown) / framerate);
        } else {

            velocity.x -= velocity.x * 10.0 * delta;
            velocity.z -= velocity.z * 10.0 * delta;

            // I might use this if I get time
            if (!canJump) {
                velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass
                camera.position.y += ( velocity.y * delta );
            }

            if ( camera.position.y < -40 && !tragedy) {
                velocity.y = 0;
                camera.position.y = -40;
                canJump = true;
            }

            // Stay a while... stayyyyy foreeeverrrrr!
            if ( Math.abs(camera.position.x) > 1250 || Math.abs(camera.position.z) > 1250 ) {
                if (!tragedy) {
                    tragedy = true;
                    canJump = false;
                    m = new Audio("themes/cybears/static/img/AAAAaaaaAAAAaaaaAAAaaa.mp3");
                    m.volume = 0.7;
                    m.loop = false;
                    m.play();
                }
                velocity.y -= 9.8 * 100.0 * delta;
                camera.position.y += ( velocity.y * delta / 20 );
            }

            direction.z = Number( moveForward ) - Number( moveBackward );
            direction.x = Number( moveLeft ) - Number( moveRight );
            direction.normalize(); // this ensures consistent movements in all directions

            if ( moveForward || moveBackward ) velocity.z -= direction.z * 400.0 * delta;
            if ( moveLeft || moveRight ) velocity.x -= direction.x * 400.0 * delta;

            camera.rotateY( -1 * velocity.x * delta / 20 );
            camera.translateZ( velocity.z * delta * 10 );
        }
    }

    prevTime = time;
    render();

}

// Render
function render() {

    if ( keyMulti > 1 ) {
        keyMulti -= Math.log(keyMulti) / 2;
    }
    cybears.rotation.y += (0.002) * keyMulti;
    decepticomTSS.rotation.y -= (0.002) * keyMulti;

    if (codeon) {
        camera.position.z += ( (mouseX/2) - camera.position.z ) * 0.002;
        camera.position.y += ( 100 + (mouseY/4) - camera.position.y ) * 0.002;
        camera.lookAt( scene.position );
    } else {
        // manual controls!
    }

    // render using our composer
    composer.render();

    // In case I want to render without the bloom pass
    // renderer.render( scene, camera );

}
