#ifndef MEM_H
#define MEM_H
#include <stdint.h>
#include <stddef.h>

void* pac_alloc(size_t size);
void pac_free(void* ptr);
#endif // MEM_H
