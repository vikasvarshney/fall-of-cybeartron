#!/usr/bin/env bash
set -o nounset
set -o errexit

SCRIPT_DIR=${0%/*}
REGISTRY_SECRET_BASENAME="${SCRIPT_DIR}/.k8s_registry_secret"

if [ $# -gt 2 ]; then
    echo "Usage: $0 [secret_name [namespace]]"
fi
SECRET_NAME=${1:-regcred}
SECRET_NAMESPACE=${2:-default}

# Work out what stashed filename we would have used for this secret
REGISTRY_SECRET_FILE="${REGISTRY_SECRET_BASENAME}.${SECRET_NAME}.yml"
# Reuse the existing secret file if it's present
if [ -f "${REGISTRY_SECRET_FILE}" ]; then
    echo "There is an existing secret file - reusing it!"
    kubectl apply --namespace "${SECRET_NAMESPACE}" -f "${REGISTRY_SECRET_FILE}"
    exit 0
fi

echo "What is the deploy token username?"
read TOKEN_NAME
echo "What is the deploy token password? (will be echoed)"
read PASSWORD

echo "Stashing the secret as a YAML file"
# Create the stashed credential YAML in no particular namespace so we can reuse
# it in any namespace later on
kubectl create secret                                                       \
    docker-registry ${SECRET_NAME}                                          \
    --docker-server=registry.gitlab.com --docker-email=deploy@cybears.io    \
    --docker-username=${TOKEN_NAME} --docker-password=${PASSWORD}           \
    --dry-run --output yaml > "${REGISTRY_SECRET_FILE}"
# Now create the secret using `kubectl apply` and specifying the namespace
echo "Creating!"
kubectl apply --namespace "${SECRET_NAMESPACE}" -f "${REGISTRY_SECRET_FILE}"

echo "The new secret is called \"${SECRET_NAME}\" in namespace \"${SECRET_NAMESPACE}\""
echo "It was stashed for later use in the file \"${REGISTRY_SECRET_FILE}\""
