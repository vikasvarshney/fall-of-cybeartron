#!/usr/bin/env python3

import base64
import requests
import argparse
import sys

def secelf_run(url, binary_path):
    with open(binary_path, 'rb') as f:
        r = requests.post(url, files={'file':f})
        if r.status_code == 200:
            j = r.json()
            return base64.b64decode(j['stdout']), base64.b64decode(j['stderr'])
        else:
            raise Exception("Failed to post! Status: {:d}".format(r.status_code))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CYBEARS SECELF Uploader')
    parser.add_argument('-u', '--url', required=True, help='The URL of the challenge server. http://secelf.ctf.cybears.io:2323/run')
    parser.add_argument('FILE', help='The file to upload')
    args = parser.parse_args()

    print("[+] Uploading {} to {}".format(args.FILE, args.url))
    stdout, stderr = secelf_run(args.url, args.FILE)

    sys.stdout.buffer.write(stdout)
    sys.stderr.buffer.write(stderr)
